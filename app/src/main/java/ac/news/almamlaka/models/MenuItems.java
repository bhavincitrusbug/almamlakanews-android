package ac.news.almamlaka.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mohammad
 * on 1/16/2018 3:29 PM.
 */

public class MenuItems implements Serializable {

    private String id;
    private String parent_id;
    private String name_ar;
    private String item_order;
    private String created_on;
    private String type_name;
    private String type_id;
    private String icon;
    private ArrayList<MenuItems> _children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getItem_order() {
        return item_order;
    }

    public void setItem_order(String item_order) {
        this.item_order = item_order;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public ArrayList<MenuItems> get_children() {
        return _children;
    }

    public void set_children(ArrayList<MenuItems> _children) {
        this._children = _children;
    }
}
