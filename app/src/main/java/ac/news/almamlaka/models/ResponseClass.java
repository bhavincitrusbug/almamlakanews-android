package ac.news.almamlaka.models;

import java.io.Serializable;

/**
 * Created by Mohammad
 * on 2/20/2018 10:10 AM.
 */

public class ResponseClass implements Serializable {
    private Boolean status;
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
