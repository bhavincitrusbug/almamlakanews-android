package ac.news.almamlaka.models;

import java.io.Serializable;

/**
 * Created by Mohammad
 * on 1/17/2018 2:00 PM.
 */

public class LiveStream implements Serializable {
    private String livestream;
    private String livestream_title;
    private String livestream_audio;
    private String livestream_description;

    public String getLivestream_audio() {

        return livestream_audio;
    }

    public void setLivestream_audio(String livestream_audio) {
        this.livestream_audio = livestream_audio;
    }

    public String getLivestream() {
        return livestream;
    }

    public void setLivestream(String livestream) {
        this.livestream = livestream;
    }

    public String getLivestream_title() {
        return livestream_title;
    }

    public void setLivestream_title(String livestream_title) {
        this.livestream_title = livestream_title;
    }

    public String getLivestream_description() {
        return livestream_description;
    }

    public void setLivestream_description(String livestream_description) {
        this.livestream_description = livestream_description;
    }
}
