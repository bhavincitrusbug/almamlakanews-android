package ac.news.almamlaka.models;

import java.io.Serializable;

/**
 * Created by Mohammad
 * on 1/3/2018 1:58 PM.
 */

public class HomePageClass implements Serializable {

    private String image;
    private String id;
    private String title;
    private Boolean isVideo;
    private String videoUrl;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getVideo() {
        return isVideo;
    }

    public void setVideo(Boolean video) {
        isVideo = video;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
