package ac.news.almamlaka.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mohammad
 * on 1/15/2018 9:44 AM.
 */

public class LeadStories implements Serializable {

    private String news_url;
    private String category_id;
    private String is_take_over;
    private String is_featured_videos;
    private String id;
    private String news_id;
    private String title;
    private String subtitle;
    private String lead_template;
    private String main_image;
    private String image_caption;
    private String slug;
    private String description;
    private String created_on;
    private ArrayList<String> images;
    private String main_image_app;
    private ArrayList<Videos> videos;
    private String videos_ids;
    private boolean has_videos;
    private String start_publish_date;
    private String author_name;
    private String categories_name;
    private boolean is_lead;

    public String getMain_image_app() {
        return main_image_app;
    }

    public void setMain_image_app(String main_image_app) {
        this.main_image_app = main_image_app;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getLead_template() {
        return lead_template;
    }

    public void setLead_template(String lead_template) {
        this.lead_template = lead_template;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getImage_caption() {
        return image_caption;
    }

    public void setImage_caption(String image_caption) {
        this.image_caption = image_caption;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public String getVideos_ids() {
        return videos_ids;
    }

    public void setVideos_ids(String videos_ids) {
        this.videos_ids = videos_ids;
    }

    public boolean isHas_videos() {
        return has_videos;
    }

    public void setHas_videos(boolean has_videos) {
        this.has_videos = has_videos;
    }

    public String getStart_publish_date() {
        return start_publish_date;
    }

    public void setStart_publish_date(String start_publish_date) {
        this.start_publish_date = start_publish_date;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getCategories_name() {
        return categories_name;
    }

    public void setCategories_name(String categories_name) {
        this.categories_name = categories_name;
    }

    public boolean isIs_lead() {
        return is_lead;
    }

    public void setIs_lead(boolean is_lead) {
        this.is_lead = is_lead;
    }

    public ArrayList<Videos> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Videos> videos) {
        this.videos = videos;
    }

    public String getNews_url() {
        return news_url;
    }

    public void setNews_url(String news_url) {
        this.news_url = news_url;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getIs_take_over() {
        return is_take_over;
    }

    public void setIs_take_over(String is_take_over) {
        this.is_take_over = is_take_over;
    }

    public String getIs_featured_videos() {
        return is_featured_videos;
    }

    public void setIs_featured_videos(String is_featured_videos) {
        this.is_featured_videos = is_featured_videos;
    }

}
