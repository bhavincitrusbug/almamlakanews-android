package ac.news.almamlaka.models;

import java.io.Serializable;

/**
 * Created by alialquraan on 2/14/18.
 */

public class NotificationCategory implements Serializable {

    private String cat_id;
    private String name;

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
