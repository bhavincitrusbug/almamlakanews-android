package ac.news.almamlaka.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mohammad
 * on 1/15/2018 9:38 AM.
 */

class FetureNews implements Serializable {

    private String id;
    private String title;
    private String subtitle;
    private String mainImage;
    private String imageCaption;
    private String slug;
    private String description;
    private String createdOn;
    private String images;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getImageCaption() {
        return imageCaption;
    }

    public void setImageCaption(String imageCaption) {
        this.imageCaption = imageCaption;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public ArrayList<Videos> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Videos> videos) {
        this.videos = videos;
    }

    public String getVideosIds() {
        return videosIds;
    }

    public void setVideosIds(String videosIds) {
        this.videosIds = videosIds;
    }

    public Integer getHasVideos() {
        return hasVideos;
    }

    public void setHasVideos(Integer hasVideos) {
        this.hasVideos = hasVideos;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    private ArrayList<Videos> videos;
    private String videosIds;
    private Integer hasVideos;
    private String url;
    private String postType;

}
