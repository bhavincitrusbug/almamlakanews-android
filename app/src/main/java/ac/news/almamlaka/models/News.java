package ac.news.almamlaka.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mohammad
 * on 1/15/2018 9:37 AM.
 */

public class News implements Serializable {

    private String breaking_news;
    private String main_sections;
    private String enabled;
    private String deleted;
    private String deleted_by;
    private String created_by;
    private String modified_on;
    private String modified_by;
    private String author_id;
    private String tags;
    private String quotation;
    private String watermark;
    private String interval_news;
    private String notes_author;
    private String lead_sections;
    private String version_number;
    private String keywords;
    private String first_section_parts;
    private String third_section_parts;
    private String news_views_count;
    private String breaking_duration;
    private String id;
    private String news_id;
    private String title;
    private String subtitle;
    private String lead_template;
    private String main_image;
    private String main_image_app;
    private String image_caption;
    private String slug;
    private String description;
    private String created_on;
    private ArrayList<String> images;
    private String news_model;
    private ArrayList<Videos> videos;
    private String videos_ids;
    private boolean has_videos;
    private String start_publish_date;
    private String author_name;
    private String categories_name;
    private boolean is_lead;
    private ArrayList<LeadStories> all_stories;
    private boolean is_take_over;
    private String news_url;
    private boolean is_featured_videos;

    public boolean isIs_featured_videos() {
        return is_featured_videos;
    }

    public void setIs_featured_videos(boolean is_featured_videos) {
        this.is_featured_videos = is_featured_videos;
    }

    public String getBreaking_news() {
        return breaking_news;
    }

    public void setBreaking_news(String breaking_news) {
        this.breaking_news = breaking_news;
    }

    public String getMain_sections() {
        return main_sections;
    }

    public void setMain_sections(String main_sections) {
        this.main_sections = main_sections;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getModified_by() {
        return modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getQuotation() {
        return quotation;
    }

    public void setQuotation(String quotation) {
        this.quotation = quotation;
    }

    public String getWatermark() {
        return watermark;
    }

    public void setWatermark(String watermark) {
        this.watermark = watermark;
    }

    public String getInterval_news() {
        return interval_news;
    }

    public void setInterval_news(String interval_news) {
        this.interval_news = interval_news;
    }

    public String getNotes_author() {
        return notes_author;
    }

    public void setNotes_author(String notes_author) {
        this.notes_author = notes_author;
    }

    public String getLead_sections() {
        return lead_sections;
    }

    public void setLead_sections(String lead_sections) {
        this.lead_sections = lead_sections;
    }

    public String getVersion_number() {
        return version_number;
    }

    public void setVersion_number(String version_number) {
        this.version_number = version_number;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getFirst_section_parts() {
        return first_section_parts;
    }

    public void setFirst_section_parts(String first_section_parts) {
        this.first_section_parts = first_section_parts;
    }

    public String getThird_section_parts() {
        return third_section_parts;
    }

    public void setThird_section_parts(String third_section_parts) {
        this.third_section_parts = third_section_parts;
    }

    public String getNews_views_count() {
        return news_views_count;
    }

    public void setNews_views_count(String news_views_count) {
        this.news_views_count = news_views_count;
    }

    public String getBreaking_duration() {
        return breaking_duration;
    }

    public void setBreaking_duration(String breaking_duration) {
        this.breaking_duration = breaking_duration;
    }

    public String getNews_url() {
        return news_url;
    }

    public void setNews_url(String news_url) {
        this.news_url = news_url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getLead_template() {
        return lead_template;
    }

    public void setLead_template(String lead_template) {
        this.lead_template = lead_template;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getImage_caption() {
        return image_caption;
    }

    public void setImage_caption(String image_caption) {
        this.image_caption = image_caption;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public String getNews_model() {
        return news_model;
    }

    public void setNews_model(String news_model) {
        this.news_model = news_model;
    }

    public ArrayList<Videos> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Videos> videos) {
        this.videos = videos;
    }

    public String getVideos_ids() {
        return videos_ids;
    }

    public void setVideos_ids(String videos_ids) {
        this.videos_ids = videos_ids;
    }

    public boolean isHas_videos() {
        return has_videos;
    }

    public void setHas_videos(boolean has_videos) {
        this.has_videos = has_videos;
    }

    public String getStart_publish_date() {
        return start_publish_date;
    }

    public void setStart_publish_date(String start_publish_date) {
        this.start_publish_date = start_publish_date;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getCategories_name() {
        return categories_name;
    }

    public void setCategories_name(String categories_name) {
        this.categories_name = categories_name;
    }

    public boolean isIs_lead() {
        return is_lead;
    }

    public void setIs_lead(boolean is_lead) {
        this.is_lead = is_lead;
    }

    public ArrayList<LeadStories> getAll_stories() {
        return all_stories;
    }

    public void setAll_stories(ArrayList<LeadStories> all_stories) {
        this.all_stories = all_stories;
    }

    public boolean isIs_take_over() {
        return is_take_over;
    }

    public void setIs_take_over(boolean is_take_over) {
        this.is_take_over = is_take_over;
    }

    public String getMain_image_app() {
        return main_image_app;
    }

    public void setMain_image_app(String main_image_app) {
        this.main_image_app = main_image_app;
    }
}
