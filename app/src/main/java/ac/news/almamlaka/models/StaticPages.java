package ac.news.almamlaka.models;

import java.io.Serializable;

/**
 * Created by Mohammad
 * on 1/29/2018 11:30 AM.
 */

public class StaticPages implements Serializable {
    private String id;
    private String title;
    private String description;
    private String created_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
}
