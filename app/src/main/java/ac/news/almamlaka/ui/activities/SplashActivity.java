package ac.news.almamlaka.ui.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.ResponseClass;
import ac.news.almamlaka.ui.dialogs.DialogForceUpdate;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {

    ImageView mWeb;
    LinearLayout mErrorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mWeb = findViewById(R.id.web_view_splash);

        //mWeb.getSettings().setUseWideViewPort(true);
        //mWeb.getSettings().setUseWideViewPort(true);
        //mWeb.getSettings().setLoadWithOverviewMode(true);
        //mWeb.loadDataWithBaseURL(null, "<style>img{position: fixed; width: 100%; height: 100%; left: 0; top: 0; margin: 0}</style>"+ "<img src='file:///android_asset/splash_screen.gif'/>", "text/html", "UTF-8", null);
        isOnline();
    }

    private void isOnline() {
        mErrorLayout = findViewById(R.id.no_internet_connection_layout);
        mErrorLayout.setOnClickListener(v -> isOnline());
        if (!Util.userHasInternetConnection(this)) {
            mErrorLayout.setVisibility(View.VISIBLE);
            mWeb.setVisibility(View.GONE);
        } else {
            mErrorLayout.setVisibility(View.GONE);
            mWeb.setVisibility(View.VISIBLE);
            Glide.with(this).load(R.mipmap.splash_screen).into(mWeb);
            new Util().setLocale(new PrefManager().getLanguageForDevice(this), this);

            checkForUpdate();

        }
    }

    private void checkForUpdate() {
        String version = Constants.APP_VERSION;
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        ApiInterface apiService = ServiceGenerator.createServiceNotification(ApiInterface.class);
        Call<ResponseClass> callArrayListChatActive = apiService.CheckUpdate("1", version);
        callArrayListChatActive.enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                Log.d("ermg", "onResponse: success" + response.code());

                ResponseClass responseClass = response.body();


                if (responseClass != null) {
                    if (responseClass.getStatus()) {
                        new DialogForceUpdate(SplashActivity.this, responseClass.getMessage()).show();
                    } else {
                        goMainActivity();
                    }

                } else {
                    goMainActivity();

                }


            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                t.printStackTrace();
                Log.d("ermg", "onResponse: onFailure");
            }
        });
    }


    private void goMainActivity() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            SplashActivity.this.finish();
        }, 1000);
    }
}