package ac.news.almamlaka.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.adapters.MainContainerAdapter;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.Util;
import ac.news.almamlaka.utilities.rtlViewPager.RtlViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mohammad
 * on 2/12/2018 3:01 PM.
 */

public class MainContainerFragment extends Fragment implements ViewPager.OnPageChangeListener {

    public static RtlViewPager intro_images;
    View mView;
    Call<ArrayList<News>> mCallGetNews;
    private LinearLayout mPagerIndicator;
    private int mDotsCount;
    private ImageView[] mDots;
    private MainContainerAdapter mAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_main_container, container, false);
        new Util().freeMemory();
        initView();
        getHomeFeaturedNews();
        return mView;
    }

    private void initView() {
        intro_images = mView.findViewById(R.id.pager_introduction);
        mPagerIndicator = mView.findViewById(R.id.viewPagerCountDots);
    }

    private void getHomeFeaturedNews() {
        assert getActivity() != null;
        DialogLoading mDialogLoading = new DialogLoading(getActivity(), false);
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        mCallGetNews = apiInterface.getNews();
        mCallGetNews.enqueue(new Callback<ArrayList<News>>() {
            @Override
            public void onResponse(Call<ArrayList<News>> call, Response<ArrayList<News>> response) {
                mDialogLoading.dismiss();
                if (getActivity() != null)
                    if (response.code() == 200)
                        if (response.body() != null) {
                            //add featured item to list of videos.
                            setAdapterPager(response.body());
                        }
            }

            @Override
            public void onFailure(Call<ArrayList<News>> call, Throwable t) {
                mDialogLoading.dismiss();
                t.printStackTrace();
            }
        });

    }

    public void setAdapterPager(ArrayList<News> body) {
        //FIRST Add featured item to list of videos
        mAdapter = new MainContainerAdapter(getChildFragmentManager(), AddFeaturedToVideos(body));
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        intro_images.setOnPageChangeListener(this);
        setUiPageViewController();
    }

    private ArrayList<News> AddFeaturedToVideos(ArrayList<News> body) {
        ArrayList<Videos> videosArrayList;
        for (News news : body) {
            if (news.isIs_featured_videos()) {
                videosArrayList = news.getVideos();
                Videos video = new Videos();
                video.setThumb(news.getMain_image());
                video.setLink("");
                video.setCategory_id(Constants.FEATURED);
                video.setCategory_name(news.getKeywords() != null ? news.getKeywords().isEmpty() ? getString(R.string.videos) : news.getKeywords() : "");
                video.setCreateDate(news.getStart_publish_date());
                video.setTitle(news.getTitle());
                videosArrayList.add(0, video);
                news.setVideos(videosArrayList);
                body.remove(news);
                body.add(news);
                break;
            }
        }
        return body;
    }

    private void setUiPageViewController() {
        mDotsCount = mAdapter.getCount();
        mDots = new ImageView[mDotsCount];
        for (int i = 0; i < mDotsCount; i++) {
            mDots[i] = new ImageView(getContext());
            mDots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected_item));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);
            mPagerIndicator.addView(mDots[i], params);
        }
        mDots[0].setImageDrawable(getResources().getDrawable(R.drawable.dot_selected_item));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mCallGetNews != null)
            mCallGetNews.cancel();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < mDotsCount; i++) {
            mDots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected_item));
        }
        mDots[position].setImageDrawable(getResources().getDrawable(R.drawable.dot_selected_item));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}