package ac.news.almamlaka.ui.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.VideoView;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.interfaces.CallbackRefreshVideo;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.adapters.InnerVideoReadMoreAdapter;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.CustomMediaController;
import ac.news.almamlaka.utilities.EndlessRecyclerViewScrollListener;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoListActivity extends AppCompatActivity implements CallbackRefreshVideo {

    FrameLayout frameLayout;
    InnerVideoReadMoreAdapter adapter;
    private VideoView mVideoView;
    private RecyclerView mRecyclerView;
    private DialogLoading mDialogLoading;
    private ArrayList<Videos> videosArrayList;
    private boolean isLANDSCAPE = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        initView();
        getVideosList(getArgs(), "1");
        VideoListActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        new Util().setLocale(new PrefManager().getLanguageForDevice(this), this);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d("ioubihbii", "true");
            isLANDSCAPE = false;
            frameLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            mRecyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isLANDSCAPE = true;
            frameLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            Log.d("ioubihbii", "false");
        }
    }

    @Override
    protected void onStop() {
        mVideoView.stopPlayback();
        super.onStop();
    }

    private void initView() {
        mVideoView = findViewById(R.id.activity_video_list_videoView);
        CustomMediaController mediaController = new CustomMediaController(VideoListActivity.this);
        mediaController.setListener(this::handleScreenOrientationFromClick);
        mVideoView.setMediaController(mediaController);
        frameLayout = findViewById(R.id.oremgoverogmvoremn);
        mRecyclerView = findViewById(R.id.activity_video_list_recyclerView);
        mDialogLoading = new DialogLoading(VideoListActivity.this);
        videosArrayList = new ArrayList<>();
        mDialogLoading.show();
    }

    private void getVideosList(String id, String page) {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<ArrayList<Videos>> callArrayListVideos = apiInterface.getVideosList(id, page);

        callArrayListVideos.enqueue(new Callback<ArrayList<Videos>>() {
            @Override
            public void onResponse(Call<ArrayList<Videos>> call, Response<ArrayList<Videos>> response) {
                ArrayList<Videos> mVideos = response.body();
                if (mVideos != null)
                    if (videosArrayList.size() == 0) {
                        videosArrayList = mVideos;
                        loadAdapter(videosArrayList);
                    } else {
                        videosArrayList.addAll(mVideos);
                        adapter.notifyDataSetChanged();
                    }
                try {
                    mDialogLoading.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Videos>> call, Throwable t) {
                try {
                    mDialogLoading.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private String getArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            return intent.getExtras().getString(Constants.VIDEO_TYPE_DETAILS_TAG, Constants.FEATURED);
        } else
            return Constants.FEATURED;
    }

    void handleScreenOrientationFromClick() {
        if (isLANDSCAPE) {
            isLANDSCAPE = false;
            VideoListActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            isLANDSCAPE = true;
            VideoListActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void loadAdapter(ArrayList<Videos> body) {
        adapter = new InnerVideoReadMoreAdapter(VideoListActivity.this, body, VideoListActivity.this);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(VideoListActivity.this);
        initVideoView(body.get(0).getLink());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //                if (videosArrayList.size() % 10 == 0)
                getVideosList(getArgs(), page + "");
            }
        });
    }

    @Override
    public void reloadVideoPlayer(String url) {
        mDialogLoading.show();
        new Util().setLocale(new PrefManager().getLanguageForDevice(this), this);
        initVideoView(url);
    }

    private void initVideoView(String url) {
        try {
            MainActivity.callStopAllMedia();
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(120);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (url != null)
            mVideoView.setVideoURI(Uri.parse(url));
        mVideoView.setOnErrorListener((mp, what, extra) -> {
            mDialogLoading.dismiss();
            return false;
        });

        mVideoView.setOnPreparedListener(mp -> {
            mVideoView.start();
            mDialogLoading.dismiss();
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVideoView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Util().setLocale(new PrefManager().getLanguageForDevice(this), this);
    }
}
