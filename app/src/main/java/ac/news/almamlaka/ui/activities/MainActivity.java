package ac.news.almamlaka.ui.activities;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.AlignmentSpan;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.LiveStream;
import ac.news.almamlaka.models.MenuItems;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.ui.dialogs.DialogYesNo;
import ac.news.almamlaka.ui.fragments.BookMarkedFragment;
import ac.news.almamlaka.ui.fragments.MainContainerFragment;
import ac.news.almamlaka.ui.fragments.MenuNewsListingTabFragment;
import ac.news.almamlaka.ui.fragments.NewsDetailsFragment;
import ac.news.almamlaka.ui.fragments.SettingsFragment;
import ac.news.almamlaka.ui.fragments.StaticPagesFragment;
import ac.news.almamlaka.ui.fragments.VideosSectionFragment;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.FragmentCaller;
import ac.news.almamlaka.utilities.MediaPlayerManager;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.Util;
import ac.news.almamlaka.utilities.liveStreamCtrlNotification.NotificationService;
import ac.news.almamlaka.utilities.resideMenu.ResideMenu;
import ac.news.almamlaka.utilities.resideMenu.ResideMenuItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by AbuQauod
 * on 1/3/2018 2:46 PM.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static MediaPlayerManager mMediaPlayerManager;
    @SuppressLint("StaticFieldLeak")
    static MainActivity mActivity;
    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            callStopAllMedia();
            try {
                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(120);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    ResideMenu mResideMenu;
    ResideMenuItem item;
    ArrayList<MenuItems> mMenuItemsArrayList;
    Call<ArrayList<MenuItems>> callMenuIcons;
    DialogLoading mDialogLoading;
    Call<LiveStream> callLinkStream;
    LiveStream ls;
    private LinearLayout menu_live;

    public static void callPlayerHandler() {
        callStopAllMedia();
        mMediaPlayerManager.start();

        Intent serviceIntent = new Intent(mActivity, NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        mActivity.startService(serviceIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new Util().setLocale(new PrefManager().getLanguageForDevice(this), this);
        // To make activity full screen.
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity = this;
        isNetworkAvailable();
        registerReceiver(myReceiver, new IntentFilter(MainActivity.class.getCanonicalName()));

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (callMenuIcons != null)
            callMenuIcons.cancel();
        if (callLinkStream != null)
            callLinkStream.cancel();
    }

    private void isNetworkAvailable() {
        View errorLayout = findViewById(R.id.no_internet_connection_layout);
        View mainLayout = findViewById(R.id.activity_main_layout);
        errorLayout.setOnClickListener(v -> isNetworkAvailable());
        if (!Util.userHasInternetConnection(this)) {
            errorLayout.setVisibility(View.VISIBLE);
            mainLayout.setVisibility(View.GONE);
        } else {
            errorLayout.setVisibility(View.GONE);
            mainLayout.setVisibility(View.VISIBLE);
            new FragmentCaller().callRemoveAllThenReplace(this, new MainContainerFragment());
            sideMenuInit();
            fire_base_Analytics();
            handleIntent(getIntent());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        // for //IllegalStateException, Can not perform this action after onSaveInstanceState
    }

    private void sideMenuInit() {
        // attach to current activity;
        mResideMenu = new ResideMenu(this);
        mResideMenu.setBackground(R.mipmap.background_screens);
        mResideMenu.attachToActivity(this);
        mResideMenu.addIgnoredView(findViewById(R.id.container_body));
        mResideMenu.setScaleValue(0.5f);
        mResideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
        ImageView menu_icon = findViewById(R.id.action_bar_menu_icon);
        findViewById(R.id.action_bar_menu_logo).setOnClickListener(v -> {
            new FragmentCaller().callRemoveAllThenReplace(MainActivity.this, new MainContainerFragment());
        });
        menu_live = findViewById(R.id.action_bar_menu_live);
        //myVideoView = findViewById(R.id.myVideoView);
        mDialogLoading = new DialogLoading(MainActivity.this);

        menu_icon.setOnClickListener(view -> mResideMenu.openMenu(ResideMenu.DIRECTION_RIGHT));
        menu_live.setOnClickListener(view -> {
            if (ls == null)
                getStreamingLink();
            else
                callLiveFragment(ls);
        });
        //Create menu items
        getMenuIcons();
        //changeColor(mResideMenu);
        //End menu items
    }

    private void callLiveFragment(LiveStream ls) {
        // Bundle bundle = new Bundle();
        // bundle.putInt(Constants.ACTION_BAR, menu_live.getId());
        // new FragmentCaller().fragmentAdderWithBundle(this, new LiveFragment(), bundle);
        //        ImageView imageView = findViewById(R.id.main_activity_image_view);


        //        Util.callVideoPlayer(this, ls.getLivestream());
        Context wrapper = new ContextThemeWrapper(this, R.style.AndroidSRC_TextAppearance);
        final PopupMenu popupMenu = new PopupMenu(wrapper, menu_live);
        MenuInflater menuInflater = new MenuInflater(MainActivity.this);
        menuInflater.inflate(R.menu.live_streaming, popupMenu.getMenu());

        MenuItem item1 = popupMenu.getMenu().getItem(0);
        SpannableString s = new SpannableString(getString(R.string.menu_video));

        s.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, s.length(), 0);
        item1.setTitle(s);

        MenuItem item2 = popupMenu.getMenu().getItem(1);
        SpannableString s2 = new SpannableString(getString(R.string.menu_audio));
        s2.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, s.length(), 0);
        item2.setTitle(s2);


        popupMenu.show();
        popupMenu.setGravity(Gravity.CENTER);
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.action_video) {
                Util.callVideoPlayer(this, ls.getLivestream());
            } else if (item.getItemId() == R.id.action_audio) {
                playAudioStreaming();
            }
            popupMenu.dismiss();
            return false;
        });


    }

    private void fire_base_Analytics() {
        try {
            FirebaseCrash.log("Activity created");
            FirebaseAnalytics.getInstance(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getStreamingLink() {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<LiveStream> callLinkStream = apiInterface.getLiveStreamLink();
        callLinkStream.enqueue(new Callback<LiveStream>() {
            @Override
            public void onResponse(Call<LiveStream> call, Response<LiveStream> response) {
                ls = response.body();
                if (ls != null && ls.getLivestream() != null) {
                    mMediaPlayerManager = new MediaPlayerManager(MainActivity.this, ls.getLivestream_audio());
                    callLiveFragment(ls);
                }
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<LiveStream> call, Throwable t) {
                t.printStackTrace();
                mDialogLoading.dismiss();
            }
        });
    }

    private void getMenuIcons() {
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        callMenuIcons = apiInterface.getMenuIcons();
        callMenuIcons.enqueue(new Callback<ArrayList<MenuItems>>() {
            @Override
            public void onResponse(Call<ArrayList<MenuItems>> call, Response<ArrayList<MenuItems>> response) {
                mMenuItemsArrayList = response.body();
                if (mMenuItemsArrayList != null)
                    createMenuItems(mMenuItemsArrayList);
            }

            @Override
            public void onFailure(Call<ArrayList<MenuItems>> call, Throwable t) {
                isNetworkAvailable();
            }
        });

    }

    private void playAudioStreaming() {
        if (mMediaPlayerManager != null) {
            callPlayerHandler();
        }
    }

    private void createMenuItems(ArrayList<MenuItems> body) {
        // String titles[] = {"الاردن", "رياصة", "الاقتصاد", "برامجنا", "فيديوهات", "الطقس", "السياحة بالاردن", "اخبار محلية", "اخبار دولية", "شرق اوسط", "الاعدادات", "الاخبار المختارة", "شروط الخدمة", "سياسة الخصوصية"};
        // int icon[] = {R.mipmap.side_menu_jordan, R.mipmap.side_menu_sport, R.mipmap.side_menu_economy, R.mipmap.side_menu_program, R.mipmap.side_menu_videos, R.mipmap.side_menu_weather, R.mipmap.side_menu_tourist, R.mipmap.side_menu_national_news, R.mipmap.side_menu_international, R.mipmap.side_menu_middeal_east, R.mipmap.side_menu_settings, R.mipmap.side_menu_book_mark, R.mipmap.side_menu_condition, R.mipmap.side_menu_politics};
        for (int i = 0; i < body.size(); i++) {
            item = new ResideMenuItem(this, body.get(i));
            item.setOnClickListener(this);
            mResideMenu.addMenuItem(item, ResideMenu.DIRECTION_RIGHT);
        }
    }

    @Override
    public void onBackPressed() {

        if (mResideMenu.isOpened()) {
            mResideMenu.closeMenu();
        } else {
            int fragments = getSupportFragmentManager().getBackStackEntryCount();
            if (fragments == 1) {
                // these lines if you want to go to first slider page if you pressed back button and its working only for first fragment HOME_FRAGMENT
                if (MainContainerFragment.intro_images != null) {
                    if (MainContainerFragment.intro_images.getCurrentItem() != 0) {
                        MainContainerFragment.intro_images.setCurrentItem(0);
                    } else
                        callDialogExit();
                } else
                    callDialogExit();
            } else {
                try {
                    if (getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount() - 1) != null)
                        if (!(getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount() - 1).getClass().getSimpleName().contains("SupportRequestManagerFragment")))
                            getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount() - 1).onResume();
                        else
                            getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount() - 2).onResume();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onBackPressed();
                Util.hideKeyboard(this);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        // ATTENTION: This was generated to handle app deep links "shared links".
        Uri appLinkData = intent.getData();
        if (appLinkData != null) {
            String id = appLinkData.getLastPathSegment();
            requestSingleNewsFromShare(id);
        } else {
            handleNotification(intent);
        }
    }

    private void requestSingleNewsFromShare(String id) {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<News> callLinkStream = apiInterface.getArticle(id);
        callLinkStream.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                News news = response.body();
                if (news != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.BUNDLE_ARTICLE, news);
                    new FragmentCaller().fragmentAdderWithBundle(MainActivity.this, new NewsDetailsFragment(), bundle);
                }
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                t.printStackTrace();
                mDialogLoading.dismiss();
            }
        });

    }

    private void handleNotification(Intent intent) {
        MainActivity.callStopAllMedia();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.get(Constants.NOTIFICATIONS_MESSAGE_BODY) != null) {
                RedirectPageFromNotification(String.valueOf(extras.get(Constants.NOTIFICATIONS_MESSAGE_BODY)));
            }
        }
    }

    public static void callStopAllMedia() {
        Intent serviceIntent = new Intent(mActivity, NotificationService.class);
        if (mMediaPlayerManager != null)
            mMediaPlayerManager.stop();
        serviceIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        mActivity.startService(serviceIntent);
    }

    private void RedirectPageFromNotification(String msg) {
        if (!msg.isEmpty())
            if (msg.contains("#")) {
                new DialogYesNo(this, (msg), Constants.DIALOG_TYPE_SHOW_MSG_BTN).show();
            } else {
                new DialogYesNo(this, Util.getMessage(msg), Constants.DIALOG_TYPE).show();
            }
    }

    private void callDialogExit() {
        // new DialogYesNo().show(this, getString(R.string.d_exit_dialog_title), getString(R.string.d_exit_dialog_content), true, Constants._DIALOG_EXIT_APP);
        System.exit(0);
    }

    @Override
    public void onClick(View view) {
        for (int i = 0; i < mMenuItemsArrayList.size(); i++) {
            if (view == mResideMenu.getMenuItems(ResideMenu.DIRECTION_RIGHT).get(i)) {
                handleMenuItemClickByType(mMenuItemsArrayList.get(i));
                break;
            }
        }
        mResideMenu.closeMenu();
    }

    void handleMenuItemClickByType(MenuItems menuItems) {
        Bundle bundle = new Bundle();
        switch (menuItems.getType_name()) {
            case Constants.MENU_TYPE_CAT:
                bundle.putSerializable(Constants.CAT_ID_NEWS, menuItems);
                new FragmentCaller().fragmentReplacerWithBundle(this, new MenuNewsListingTabFragment(), bundle);
                break;
            case Constants.MENU_TYPE_PAGE:
                bundle.putSerializable(Constants.STATIC_PAGE_ID, menuItems);
                new FragmentCaller().fragmentReplacerWithBundle(this, new StaticPagesFragment(), bundle);
                break;
            case Constants.MENU_TYPE_VIDEO:
                bundle.putSerializable(Constants.STATIC_PAGE_ID, menuItems);
                bundle.putSerializable(Constants.videos_section_frag_hide_Featured, true);
                new FragmentCaller().fragmentReplacerWithBundle(this, new VideosSectionFragment(), bundle);
                break;
            case Constants.MENU_TYPE_WEATHER:
                break;
            case Constants.MENU_TYPE_API:
                break;
            case Constants.MENU_TYPE_SETTINGS:
                //                bundle.putSerializable(Constants.RESIDE_MENU, mResideMenu);
                new FragmentCaller().fragmentReplacerWithBundle(this, new SettingsFragment(), bundle);
                break;
            case Constants.MENU_TYPE_BOOKMARK:
                new FragmentCaller().fragmentReplacerWithBundle(this, new BookMarkedFragment(), bundle);
                break;
        }
    }
}