package ac.news.almamlaka.ui.fragments;

import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.NotificationCategory;
import ac.news.almamlaka.models.ResponseClass;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.FragmentCaller;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.RadioButtonCustom;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.Util;
import ac.news.almamlaka.utilities.resideMenu.ResideMenu;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends Fragment {
    private View mView;
    private RadioButtonCustom rSmall, rMedium, rLarge;
    private SwitchCompat scNotificationUrgent, scNotificationLiveStream;
    private Call<ResponseClass> callArrayListChatActive;
    private Call<ArrayList<NotificationCategory>> callArrayNotificationCategory;
    // private   RecyclerView mRecyclerView;
    private ArrayList<NotificationCategory> notificationCategoryArrayList;
    private ResideMenu mResideMenu;
    private DialogLoading mDialogLoading;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_settings, container, false);
        initController();
        return mView;
    }

    private void initController() {
        mDialogLoading = new DialogLoading(getActivity());
        rSmall = mView.findViewById(R.id.settings_fragment_radio_small);
        rMedium = mView.findViewById(R.id.settings_fragment_radio_medium);
        rLarge = mView.findViewById(R.id.settings_fragment_radio_large);

        scNotificationUrgent = mView.findViewById(R.id.settings_fragment_switch_notification_urgent);
        scNotificationLiveStream = mView.findViewById(R.id.settings_fragment_switch_notification_livestream);
//        mRecyclerView = mView.findViewById(R.id.settings_fragment_rv_notifications);

        handelRadioButton();
        handelNotificationSwitch();

    }

    void handelNotificationSwitch() {
        boolean isUrgentNotificationsEnable = new PrefManager().isUrgentNotificationsEnable(getContext());
        boolean isLiveStreamNotificationsEnable = new PrefManager().isLiveStreamNotificationsEnable(getContext());

        scNotificationUrgent.setChecked(isUrgentNotificationsEnable);
        scNotificationLiveStream.setChecked(isLiveStreamNotificationsEnable);


        scNotificationUrgent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            new PrefManager().setIsUrgentNotificationsEnable(getActivity(), isChecked);
            if (isChecked) {
                sendRegistrationToServer("urgent", scNotificationUrgent);
            } else {
                unRegisterNotifications("urgent", scNotificationUrgent);
            }
        });

        scNotificationLiveStream.setOnCheckedChangeListener((buttonView, isChecked) -> {
            new PrefManager().setIsLiveStreamNotificationsEnable(getActivity(), isChecked);
            if (isChecked) {
                sendRegistrationToServer("livestream", scNotificationLiveStream);
            } else {
                unRegisterNotifications("livestream", scNotificationLiveStream);
            }
        });


    }

    private void handelRadioButton() {
        checkStatus();
        rSmall.setOnClickListener(v -> {
            rMedium.setChecked(false);
            rLarge.setChecked(false);
            new PrefManager().setFontSize(getActivity(), Constants.FONT_SIZE_SMALL);
            checkStatus();
            //      changeFontSize(Constants.FONT_SIZE_SMALL);
        });

        rMedium.setOnClickListener(v -> {

            rSmall.setChecked(false);
            rLarge.setChecked(false);
            new PrefManager().setFontSize(getActivity(), Constants.FONT_SIZE_MEDIUM);
            checkStatus();
            //  changeFontSize(Constants.FONT_SIZE_MEDIUM);
        });

        rLarge.setOnClickListener(v -> {
            rSmall.setChecked(false);
            rMedium.setChecked(false);
            new PrefManager().setFontSize(getActivity(), Constants.FONT_SIZE_LARGE);
            checkStatus();
            // changeFontSize(Constants.FONT_SIZE_LARGE);
        });
    }

//    private void handelNotificationSwitch() {
//        boolean isNotificationsEnable = new PrefManager().isNotificationsEnable(getContext());
//
//        scNotification.setChecked(isNotificationsEnable);
//        if (isNotificationsEnable) {
//            getNotificationCategory();
//        } else {
//            mRecyclerView.setVisibility(View.GONE);
//        }
//
//        scNotification.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            new PrefManager().setIsNotificationsEnable(getActivity(), isChecked);
//            if (isChecked) {
//                //sendRegistrationToServer();
//                getNotificationCategory();
//            } else {
//                // unRegisterNotifications();
//                mRecyclerView.setVisibility(View.GONE);
//            }
//        });
//    }

    void checkStatus() {
        if (new PrefManager().getFontSize(getActivity()) == Constants.FONT_SIZE_SMALL) {
            rSmall.setChecked(true);
            rMedium.setChecked(false);
            rLarge.setChecked(false);
        } else if (new PrefManager().getFontSize(getActivity()) == Constants.FONT_SIZE_LARGE) {
            rSmall.setChecked(false);
            rMedium.setChecked(false);
            rLarge.setChecked(true);
        } else {
            rSmall.setChecked(false);
            rMedium.setChecked(true);
            rLarge.setChecked(false);
        }
    }

//   private void getNotificationCategory() {
//       DialogLoading mDialogLoading = new DialogLoading(getContext());
//       mDialogLoading.show();
//       ApiInterface apiService = ServiceGenerator.createServiceNotification(ApiInterface.class);
//       callArrayNotificationCategory = apiService.getNotificationCategoryList();
//       callArrayNotificationCategory.enqueue(new Callback<ArrayList<NotificationCategory>>() {
//           @Override
//           public void onResponse(Call<ArrayList<NotificationCategory>> mCallGetNews, Response<ArrayList<NotificationCategory>> response) {
//               Log.d("get Notifications", response.body() + " " + response.code());
//               notificationCategoryArrayList = response.body();
//               if (notificationCategoryArrayList != null && notificationCategoryArrayList.size() > 0) {
//                   mRecyclerView.setVisibility(View.VISIBLE);
//                   mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                   mRecyclerView.setNestedScrollingEnabled(false);
//                   mRecyclerView.setAdapter(new SettingsCategoryListAdapter(getActivity(), notificationCategoryArrayList));

//               }
//               mDialogLoading.dismiss();
//           }

//           @Override
//           public void onFailure(Call<ArrayList<NotificationCategory>> mCallGetNews, Throwable t) {
//               Log.d("njnoln", t.getMessage() + "");
//               mDialogLoading.dismiss();
//           }
//       });
//   }

    @Override
    public void onStop() {
        super.onStop();
        if (callArrayListChatActive != null)
            callArrayListChatActive.cancel();
    }

    private void sendRegistrationToServer(String catID, SwitchCompat switchCompat) {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<ResponseClass> registerByCat = apiInterface.registerByCat(new PrefManager().getRegToken(getActivity()), Util.getUniquePhoneIdentity(), Constants.APP_ID,
                Constants.PLATFORM_ID, catID, Constants.AX_API_key, Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
        registerByCat.enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                ResponseClass body = response.body();
                if (body != null)
                    if (body.getStatus())
                        new PrefManager().deleteDisabledNotificationsCategoryIDs(getActivity(), catID);
                    else
                        switchCompat.setChecked(false);
                Log.d("register1", "onResponse: success" + response.code());
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                mDialogLoading.dismiss();
                Log.d("register1", "onFailure: success");

            }
        });
    }


    private void unRegisterNotifications(String catID, SwitchCompat switchCompat) {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<ResponseClass> mCallUnregisterByTopics = apiInterface.unregisterByCat(Constants.APP_ID, catID, Constants.PLATFORM_ID, Constants.AX_API_key, Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
        mCallUnregisterByTopics.enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                ResponseClass body = response.body();
                if (body != null)
                    if (body.getStatus()) {
                        new PrefManager().setDisabledNotificationsCategoryIDs(getActivity(), catID);
                    } else
                        switchCompat.setChecked(false);
                Log.d("register1", "onResponse: success" + response.code());
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                mDialogLoading.dismiss();
                Log.d("register1", "onFailure: success");
            }
        });
    }


    private void changeFontSize(int fontSizeLarge) {
        new FragmentCaller().ReloadCurrentFragment(getContext(), this);

        if (mResideMenu == null)
            mResideMenu = getResideMenu();

        for (int i = 0; i < mResideMenu.getMenuItems(ResideMenu.DIRECTION_RIGHT).size(); i++) {
            TextViewCustom custom = mResideMenu.getMenuItems(ResideMenu.DIRECTION_RIGHT).get(i).getTitle();
            custom.setTextSize(Math.abs(getMyTextSize(custom)));
        }
    }

    private ResideMenu getResideMenu() {
        if (getArguments() != null)
            return (ResideMenu) getArguments().getSerializable(Constants.RESIDE_MENU);
        else
            return new ResideMenu(getContext());

    }

    float getMyTextSize(TextViewCustom custom) {
        return custom.getTextSize() / getResources().getDisplayMetrics().scaledDensity + new PrefManager().getFontSize(this.getContext());
    }
}
