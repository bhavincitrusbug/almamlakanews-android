package ac.news.almamlaka.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.MenuItems;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.adapters.NewsListAdapter;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.EndlessRecyclerViewScrollListener;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mohammad
 * on 1/10/2018 8:18 AM.
 */

public class MenuNewsListingFragment extends Fragment {

    View mView;
    RecyclerView mRecyclerView;
    Call<ArrayList<News>> callNewsByCategoryID;
    LinearLayoutManager mLinearLayoutManager;
    MenuItems mChildrenMenu;
    DialogLoading mDialogLoading;
    ProgressBar mDialogLoadingPaging;
    NewsListAdapter adapter;
    private ArrayList<News> mMainNewsList;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_news_listing, container, false);
        initView();
        new Util().freeMemory();
        mDialogLoading.show();
        setUpRecyclerView();
        fetchHomeTimeline(1, 99);

        return mView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (callNewsByCategoryID != null)
            callNewsByCategoryID.cancel();
    }

    private void setUpRecyclerView() {
        mMainNewsList = new ArrayList<>();
        NewsListAdapter newsListAdapter = new NewsListAdapter(getActivity(), mMainNewsList);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(newsListAdapter);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                fetchHomeTimeline(page, 99);

            }
        });
    }

    private int getCatID() {
        if (getArguments() != null)
            mChildrenMenu = (MenuItems) getArguments().getSerializable(Constants.CAT_ID_NEWS);
        if (mChildrenMenu != null) {
            return Integer.parseInt(mChildrenMenu.getType_id());
        } else
            return 0;
    }

    private void initView() {
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView = mView.findViewById(R.id.fragment_news_listing_recycler);
        mDialogLoadingPaging = mView.findViewById(R.id.fragment_news_listing_pg);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (getActivity() != null)
            mDialogLoading = new DialogLoading(getActivity());
    }

    private void loadAdapter() {
        adapter = new NewsListAdapter(getContext(), mMainNewsList);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
    }

    private void fetchHomeTimeline(final int page, long maxId) {
        if (page != 1) {
            mDialogLoadingPaging.setVisibility(View.VISIBLE);
        }
        if (page <= maxId) {

            ApiInterface serviceGenerator = ServiceGenerator.createService(ApiInterface.class);
            callNewsByCategoryID = serviceGenerator.getNewsByCategoryID(getCatID(), page);
            callNewsByCategoryID.enqueue(new Callback<ArrayList<News>>() {
                @Override
                public void onResponse(Call<ArrayList<News>> call, Response<ArrayList<News>> response) {
                    ArrayList<News> tempNewsList = response.body();
                    if (tempNewsList != null) {
                        if (page == 1) {
                            mMainNewsList = tempNewsList;
                            loadAdapter();
                        } else {
                            mMainNewsList.addAll(tempNewsList);
                            adapter.notifyDataSetChanged();
                        }
                    }
                    if (isAdded()) {
                        mDialogLoadingPaging.setVisibility(View.GONE);
                        mDialogLoading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<News>> call, Throwable t) {
                    t.printStackTrace();
                    if (isAdded()) {
                        mDialogLoading.dismiss();
                        mDialogLoadingPaging.setVisibility(View.GONE);
                    }
                }
            });
        }
    }
}
