package ac.news.almamlaka.ui.fragments;

/*
 * Created by Mohammad
 * on 1/3/2018 2:45 PM.
 */

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.FragmentCaller;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.TextViewCustomBold;
import ac.news.almamlaka.utilities.Util;

public class HomePagerFragment extends Fragment {
    View mView;
    int mPosition = 0;
    private ImageView mImageView;
    private LinearLayout mNormalLin;
    private LinearLayout mLeadLin;
    private LinearLayout mSocialLin;
    private TextViewCustom mLeadCat;
    private TextViewCustomBold mLeadTitle;

    // newInstance constructor for creating fragment with arguments
    public static HomePagerFragment newInstance(News body, int position) {
        HomePagerFragment fragmentFirst = new HomePagerFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.HOME_PAGER_ITEM, body);
        args.putInt(Constants.HOME_PAGER_ITEM_POS, position);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.item_pager, container, false);
        new Util().freeMemory();
        test(getArgs(), mView);
        return mView;
    }

    private void test(News item, View itemView) {
        mImageView = mView.findViewById(R.id.img_pager_item);
        ImageView mShare = mView.findViewById(R.id.item_feature_share);
        ImageView mBookMark = mView.findViewById(R.id.item_feature_book_mark);
        ImageView mVideoIcon = mView.findViewById(R.id.item_feature_video_icon);
        TextView mCat = mView.findViewById(R.id.item_feature_cat);
        TextView mTitle = mView.findViewById(R.id.item_feature_title);
        TextView mDesc = mView.findViewById(R.id.item_feature_desc);
        TextView mTime = mView.findViewById(R.id.item_feature_read_time);
        mNormalLin = mView.findViewById(R.id.item_feature_lin);
        mLeadLin = mView.findViewById(R.id.item_feature_lead_story_lin);
        mSocialLin = mView.findViewById(R.id.home_pager_social_lin);
        mLeadCat = mView.findViewById(R.id.item_feature_lead_story_cat);
        mLeadTitle = mView.findViewById(R.id.item_feature_lead_story_title);

        mSocialLin.setVisibility(View.VISIBLE);
        mCat.setText(item.getKeywords());
        mTitle.setText(item.getTitle());
        mDesc.setText(item.getSubtitle());
        mTime.setText((item.getStart_publish_date()));
        mTime.setVisibility(View.GONE);
        if (getContext() != null)
            Glide.with(getContext()).load(Constants.IMAGE_PATH + item.getMain_image()).into(mImageView);

        Util.addOnClickAnimation(itemView);

        itemView.setOnClickListener(v -> {
            if (item.isIs_take_over()) {
                if (item.isHas_videos())
                    callVideo(item.getVideos());
            } else //if (item.isIs_lead())
                callWithArgs(item, item.isIs_lead());
            // else if (item.isHas_videos())
            //     callVideo(item.getVideos());
            // else
            //     callWithArgs(mNews.get(mPosition), item.isIs_lead());

        });



        mVideoIcon.setOnClickListener(v -> callVideo(item.getVideos()));
        mShare.setOnClickListener(v -> new Util().Share(getContext(), item.getNews_url()));
        mBookMark.setOnClickListener(v -> new Util().BookedMarkArticles(getContext(), item));

        checkIfVideo(item, mVideoIcon);
        checkIfItsLead(item);
        setShareAndBookMarkIconColor(mPosition, mShare, mBookMark);
    }

    private News getArgs() {
        if (getArguments() != null)
            mPosition = getArguments().getInt(Constants.HOME_PAGER_ITEM_POS);

        return (News) getArguments().getSerializable(Constants.HOME_PAGER_ITEM);
    }

    private void callVideo(ArrayList<Videos> homePageClass) {
        //Feras said that no way to be more than one video in the home pager
        if (homePageClass != null && homePageClass.size() > 0)
            Util.callVideoPlayer((AppCompatActivity) getContext(), homePageClass.get(0).getLink());
    }

    private void callWithArgs(News news, boolean is_lead) {

        Bundle bundle = new Bundle();                                     //Handle adding the main image to the images
        // ArrayList<String> images = news.getImages();                    //Handle adding the main image to the images
        // images.add(news.getMain_image());                               //Handle adding the main image to the images
        // news.setImages(images);                                         //Handle adding the main image to the images

        bundle.putSerializable(Constants.BUNDLE_ARTICLE, news);
        if (is_lead)
            new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) getContext(), new LeadStoryFragment(), bundle);
        else
            new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) getContext(), new NewsDetailsFragment(), bundle);
        // news.getImages().remove(news.getImages().size()-1);      //Handle adding the main image to the images
    }

    private void checkIfVideo(News item, ImageView mVideoIcon) {
        // check if video
        if (item.isHas_videos())
            if (!item.isIs_lead())
                mVideoIcon.setVisibility(View.VISIBLE);
            else
                mVideoIcon.setVisibility(View.GONE);
    }

    private void checkIfItsLead(News item) {
        // check if its lead
        if (item.isIs_take_over()) {
            mLeadLin.setVisibility(View.GONE);
            mNormalLin.setVisibility(View.GONE);
            mSocialLin.setVisibility(View.GONE);
            if (getContext() != null)
                if (item.getVideos() != null && item.getVideos().size() > 0)
                    if (item.isHas_videos())
                        Glide.with(getContext()).load(Constants.BASE_IMAGE_VIDEO_THUMB + item.getVideos().get(0).getThumb()).into(mImageView);
                    else
                        Glide.with(getContext()).load(Constants.BASE_IMAGE_VIDEO_THUMB + item.getMain_image_app()).into(mImageView);
        } else if (item.isIs_lead()) {
            mNormalLin.setVisibility(View.GONE);
            mLeadLin.setVisibility(View.VISIBLE);
            mLeadCat.setText(item.getKeywords());
            mLeadTitle.setText(item.getTitle());
            // check the lead template
            if (getContext() != null)
                if (item.getLead_template().equals("1"))
                    mLeadLin.setBackgroundResource(R.mipmap.lead_story_background);
                else
                    mLeadLin.setBackground(getContext().getResources().getDrawable(R.color.text_color));
        } else {
            mLeadLin.setVisibility(View.GONE);
            mNormalLin.setVisibility(View.VISIBLE);
        }

    }

    private void setShareAndBookMarkIconColor(int position, ImageView mShare, ImageView mBookMark) {
        if (getContext() != null)
        //   if (position == 0) {
        //       Glide.with(getContext()).load(R.mipmap.live_screen_share).into(mShare);
        //       Glide.with(getContext()).load(R.mipmap.white_bookmark).into(mBookMark);
        //   } else {
                Glide.with(getContext()).load(R.mipmap.share_feature).into(mShare);
                Glide.with(getContext()).load(R.mipmap.book_mark).into(mBookMark);
        //    }
    }

}