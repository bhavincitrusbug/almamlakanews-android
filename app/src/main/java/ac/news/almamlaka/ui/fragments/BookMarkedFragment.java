package ac.news.almamlaka.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.adapters.BookMarkAdapter;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.RecyclerItemTouchHelper;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.Util;

/**
 * Created by Mohammad
 * on 1/10/2018 8:18 AM.
 */

public class BookMarkedFragment extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    View mView;
    RecyclerView mRecyclerView;
    TextViewCustom mTextViewError;
    private BookMarkAdapter mAdapter;
    private List<News> mNewsList;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_book_mark, container, false);
        initView();
        new Util().freeMemory();
        loadAdapter();
        return mView;
    }

    private void initView() {
        mRecyclerView = mView.findViewById(R.id.fragment_book_mark_recycler);
        mTextViewError = mView.findViewById(R.id.fragment_book_mark_error_msg);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void loadAdapter() {
        if (getData(getContext()).size() == 0) {
            mRecyclerView.setVisibility(View.GONE);
            mTextViewError.setVisibility(View.VISIBLE);
            return;
        }
        mRecyclerView.setVisibility(View.VISIBLE);
        mTextViewError.setVisibility(View.GONE);

        mNewsList = getData(getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        if (getContext() != null)
            mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mAdapter = new BookMarkAdapter(getContext(), mNewsList);
        mRecyclerView.setAdapter(mAdapter);

        // adding mMenuItems touch helper
        // only ItemTouchHelper.LEFT added to detect Right to Left swipe
        // if you want both Right -> Left and Left -> Right
        // add pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT as param
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);

    }

    private ArrayList<News> getData(Context mContext) {
        return new PrefManager().getBookMarkedItem(mContext);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof BookMarkAdapter.MyViewHolder) {
            // get the removed mMenuItems mCat to display it in snack bar
            String name = mNewsList.get(viewHolder.getAdapterPosition()).getTitle();

            // backup of removed mMenuItems for undo purpose
            final News deletedItem = mNewsList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the mMenuItems from recycler view
            mAdapter.removeItem(viewHolder.getAdapterPosition());
            new PrefManager().deleteBookMarkedItem(getActivity(), deletedItem);

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar.make(mRecyclerView, getString(R.string.removed_done, name), Snackbar.LENGTH_LONG);
            snackbar.setAction(getString(R.string.undo), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // undo is selected, restore the deleted mMenuItems
                    mAdapter.restoreItem(deletedItem, deletedIndex);
                    new Util().BookedMarkArticles(getActivity(), deletedItem);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
}
