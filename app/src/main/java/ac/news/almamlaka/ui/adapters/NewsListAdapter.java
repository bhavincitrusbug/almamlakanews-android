package ac.news.almamlaka.ui.adapters;

/**
 * Created by Mohammad
 * on 1/9/2018 9:39 AM.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.fragments.NewsDetailsFragment;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.FragmentCaller;
import ac.news.almamlaka.utilities.TextViewCustom;

public class NewsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<News> mNews;
    private Context mContext;

    public NewsListAdapter(Context context, ArrayList<News> results) {
        this.mNews = results;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case Constants.ITEM_FEATURED:
                itemView = View.inflate(parent.getContext(), R.layout.item_list_news_featured, null);
                viewHolder = new FeaturedViewHolder(itemView);
                break;
            case Constants.ITEM_NORMAL:
                itemView = View.inflate(parent.getContext(), R.layout.item_list_news_normal, null);
                viewHolder = new NormalViewHolder(itemView);
                break;
        }
        RecyclerView.ViewHolder finalViewHolder = viewHolder;

        if (itemView != null) {
            itemView.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.BUNDLE_ARTICLE, mNews.get(finalViewHolder.getAdapterPosition()));
                new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) mContext, new NewsDetailsFragment(), bundle);
            });
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(holder.getAdapterPosition());
        News review = mNews.get(holder.getAdapterPosition());
        switch (viewType) {
            case Constants.ITEM_FEATURED:
                FeaturedViewHolder featuredViewHolder = (FeaturedViewHolder) holder;
                Glide.with(mContext).load(Constants.IMAGE_PATH + review.getMain_image()).into(featuredViewHolder.mImageViewF);
                featuredViewHolder.mName.setText(review.getTitle());
                featuredViewHolder.mDesc.setText(review.getSubtitle());
                featuredViewHolder.mDate.setText((review.getStart_publish_date()));
                break;
            case Constants.ITEM_NORMAL:
                NormalViewHolder normalViewHolder = (NormalViewHolder) holder;
                Glide.with(mContext).load(Constants.IMAGE_PATH + review.getMain_image()).into(normalViewHolder.mImageView);
                normalViewHolder.mName.setText(review.getTitle());
                normalViewHolder.mCat.setText(review.getKeywords());
                normalViewHolder.mDate.setText((review.getStart_publish_date()));
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? Constants.ITEM_FEATURED : Constants.ITEM_NORMAL;
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    class FeaturedViewHolder extends RecyclerView.ViewHolder {
        TextViewCustom mName, mDesc, mDate;
        ImageView mImageViewF;

        FeaturedViewHolder(View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.item_list_news_featured_name);
            mDesc = itemView.findViewById(R.id.item_list_news_featured_desc);
            mDate = itemView.findViewById(R.id.item_list_news_featured_news_time);
            mImageViewF = itemView.findViewById(R.id.item_list_news_featured_image);
        }
    }

    class NormalViewHolder extends RecyclerView.ViewHolder {
        TextViewCustom mCat, mDate, mName;
        ImageView mImageView;

        NormalViewHolder(View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.item_list_news_normal_name);
            mCat = itemView.findViewById(R.id.item_list_news_normal_cat);
            mDate = itemView.findViewById(R.id.item_list_news_normal_time);
            mImageView = itemView.findViewById(R.id.item_list_news_normal_image);
        }
    }
}

