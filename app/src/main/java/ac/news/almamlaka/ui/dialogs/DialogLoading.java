package ac.news.almamlaka.ui.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;

import ac.news.almamlaka.R;


/**
 * Created by alialquraan
 * on 1/18/18.
 */

public class DialogLoading extends Dialog {

    private boolean isCancelable = false;

    public DialogLoading(@NonNull Context context) {
        super(context);
    }

    public DialogLoading(@NonNull Context context, boolean isCancelable) {
        super(context);
        this.isCancelable = isCancelable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null)
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_loading);
        this.setCanceledOnTouchOutside(isCancelable);
    }
}
