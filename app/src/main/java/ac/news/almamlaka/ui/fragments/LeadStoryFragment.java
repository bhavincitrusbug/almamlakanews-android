package ac.news.almamlaka.ui.fragments;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.LeadStories;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.adapters.ArticlePagerAdapter;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.TextViewCustomBold;
import ac.news.almamlaka.utilities.Util;
import ac.news.almamlaka.utilities.rtlViewPager.RtlViewPager;

import static java.lang.Math.abs;
import static java.lang.Math.floor;
import static java.lang.Math.log10;
import static java.lang.Math.pow;

/**
 * Created by Mohammad
 * on 1/10/2018 8:18 AM.
 */

public class LeadStoryFragment extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener {
    View mView;
    News mArticle;
    LinearLayout mPagerIndicator;
    int mDotsCount;
    ImageView[] mDots;
    ArticlePagerAdapter mAdapter;
    ImageView mImageView;
    RelativeLayout mViewpagerHolder;
    LinearLayout main_bg, dynamic_lin;
    FloatingActionMenu mFab;
    TextViewCustom mDate, mCat;
    TextViewCustomBold mTitle;
    WebView mDesc;
    HorizontalScrollView mHorizontalScrollView;
    int mIDToSaveBookMark = 0;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_lead_story, container, false);
        new Util().freeMemory();
        getArgs();
        loadFabMenu();
        return mView;
    }

    private void getArgs() {
        Bundle bundle = getArguments();
        mArticle = (News) bundle.getSerializable(Constants.BUNDLE_ARTICLE);
        if (mArticle != null)
            loadArticle();
        else
            Util.showErrorView(getContext());
    }

    void loadFabMenu() {
        FloatingActionButton menuShare = mView.findViewById(R.id.subFloatingMenuShare);
        FloatingActionButton menuBookmark = mView.findViewById(R.id.subFloatingMenuBookMark);

        menuShare.setOnClickListener(v -> {
            if (mArticle.isIs_lead()) {
                new Util().Share(getContext(), mArticle.getAll_stories().get(mIDToSaveBookMark).getNews_url());
            } else {
                new Util().Share(getContext(), mArticle.getNews_url());
            }
        });
        menuBookmark.setOnClickListener(v -> {
            if (mArticle.isIs_lead()) {
                saveBookMarkForLeadStory(mArticle.getAll_stories().get(mIDToSaveBookMark));
            } else {
                new Util().BookedMarkArticles(getContext(), mArticle);
            }
        });
    }

    private void loadArticle() {
        mImageView = mView.findViewById(R.id.article_header_image);
        mDate = mView.findViewById(R.id.article_date);
        mTitle = mView.findViewById(R.id.article_title);
        mCat = mView.findViewById(R.id.article_cat);
        mDesc = mView.findViewById(R.id.article_desc);

        mViewpagerHolder = mView.findViewById(R.id.article_slider);
        main_bg = mView.findViewById(R.id.fragment_lead_story_rel);

        if (mArticle.getLead_template().equals("1"))
            main_bg.setBackgroundResource(R.mipmap.lead_story_background);
        else
            main_bg.setBackground(getResources().getDrawable(R.color.text_color));

        mFab = mView.findViewById(R.id.FloatingActionMenu1);
        dynamic_lin = mView.findViewById(R.id.fragment_lead_story_tab_titles);
        mHorizontalScrollView = mView.findViewById(R.id.fragment_lead_story_tab_scroll);

        loadDynamicButton(mArticle);
        setupPageContent(mArticle.getAll_stories().get(0));
    }

    private void saveBookMarkForLeadStory(LeadStories article) {
        News news = new News();
        news.setId(article.getId());
        news.setNews_id(article.getNews_id());
        news.setTitle(article.getTitle());
        news.setSubtitle(article.getSubtitle());
        news.setLead_template(article.getLead_template());
        news.setMain_image(article.getMain_image());
        news.setImage_caption(article.getImage_caption());
        news.setSlug(article.getSlug());
        news.setDescription(article.getDescription());
        news.setCreated_on(article.getCreated_on());
        news.setImages(article.getImages());
        news.setMain_image_app(article.getMain_image_app());
        news.setVideos(article.getVideos());
        news.setVideos_ids(article.getVideos_ids());
        news.setHas_videos(article.isHas_videos());
        news.setStart_publish_date(article.getStart_publish_date());
        news.setAuthor_name(article.getAuthor_name());
        news.setCategories_name(article.getCategories_name());
        news.setIs_lead(article.isIs_lead());
        new Util().BookedMarkArticles(getContext(), news);

    }

    private void loadDynamicButton(News article) {
        for (int i = 0; i < article.getAll_stories().size(); i++) {
            dynamic_lin.addView(createTextView(i, article));
        }
    }

    private void setupPageContent(LeadStories leadStories) {
        mDate.setText((mArticle.getStart_publish_date()));
        mTitle.setText(leadStories.getTitle());
        mCat.setText(leadStories.getCategories_name());
        //mDesc.setText(leadStories.getDescription());
        //        new Util().setDataInWebView(mDesc, leadStories.getDescription());
        new Util().setDataInWebViewWithSize(mDesc, leadStories.getDescription(), new PrefManager().getFontSize(getActivity()));
        loadSliderOrVideo(leadStories);
    }

    private View createTextView(int i, News article) {
        TextViewCustom myText = new TextViewCustom(getContext());
        myText.setText(article.getAll_stories().get(i).getTitle());
        if (i == 0)
            myText.setBackgroundResource(R.drawable.under_line_colored);
        myText.setLines(2);
        myText.setPadding(8, 20, 80, 20);
        myText.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        myText.setOnClickListener(this);

        //width and height
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(500, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(25, 10, 25, 0);
        myText.setLayoutParams(params);

        return myText;
    }

    private void loadSliderOrVideo(LeadStories leadStories) {
        if (leadStories.getImages() != null)
            if (leadStories.getImages().size() == 0 && leadStories.getVideos().size() == 0) {
                mViewpagerHolder.setVisibility(View.GONE);
                mImageView.setVisibility(View.VISIBLE);
                if (getActivity() != null) {
                    Glide.with(getActivity()).load(Constants.IMAGE_PATH + leadStories.getMain_image()).into(mImageView);
                }
            } else {
                mViewpagerHolder.setVisibility(View.VISIBLE);
                mImageView.setVisibility(View.GONE);
                loadTheSlider(leadStories);
            }
    }

    public void loadTheSlider(LeadStories leadStories) {
        RtlViewPager mViewPagerIntroImages = mView.findViewById(R.id.pager_article);
        mPagerIndicator = mView.findViewById(R.id.viewPagerCountDots);
        mAdapter = new ArticlePagerAdapter(getContext(), mImagesAndVideosArray(leadStories));
        mViewPagerIntroImages.setAdapter(mAdapter);
        mViewPagerIntroImages.setCurrentItem(0);
        mViewPagerIntroImages.setOnPageChangeListener(this);
        setUiPageViewController();
    }

    private List<Videos> mImagesAndVideosArray(LeadStories leadStories) {

        ArrayList<Videos> mImagesAndVideosArray = new ArrayList<>();

        Videos i = new Videos();
        i.setLink("");
        i.setThumb(leadStories.getMain_image());
        mImagesAndVideosArray.add(i);


        mImagesAndVideosArray.addAll(leadStories.getVideos());
        return mImagesAndVideosArray;
    }

    private void setUiPageViewController() {
        mDotsCount = mAdapter.getCount();
        mDots = new ImageView[mDotsCount];
        mPagerIndicator.removeAllViews();
        for (int i = 0; i < mDotsCount; i++) {
            mDots[i] = new ImageView(getContext());
            mDots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected_item));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);
            mPagerIndicator.addView(mDots[i], params);
        }
        mDots[0].setImageDrawable(getResources().getDrawable(R.drawable.dot_selected_item));
    }

    private int getHeightInt(double input) {
        double tmp = abs(input);
        tmp *= pow(10, abs(floor(log10(tmp))));
        return (int) tmp;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < mDotsCount; i++) {
            mDots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected_item));
        }
        mDots[position].setImageDrawable(getResources().getDrawable(R.drawable.dot_selected_item));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onClick(View v) {
        TextViewCustom mTemp;
        for (int i = 0; i < dynamic_lin.getChildCount(); i++)
            if (dynamic_lin.getChildAt(i) instanceof TextViewCustom) {
                mTemp = ((TextViewCustom) dynamic_lin.getChildAt(i));
                if (mTemp.getText() == ((TextViewCustom) v).getText()) {
                    mTemp.setSelected(true);
                    dynamic_lin.scrollTo(0, (int) dynamic_lin.getY());
                    mTemp.setBackgroundResource(R.drawable.under_line_colored);
                    setupPageContent(mArticle.getAll_stories().get(i));
                    scrollTo(mTemp);
                    mIDToSaveBookMark = i;
                } else {
                    mTemp.setSelected(false);
                    mTemp.setBackgroundResource(R.drawable.under_line_white);
                }
            }
    }

    private void scrollTo(TextViewCustom mTemp) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int x, y;
        x = mTemp.getLeft() - (width / 4);
        y = mTemp.getTop();
        //mHorizontalScrollView.scrollTo(x, y);

        ObjectAnimator animator = ObjectAnimator.ofInt(mHorizontalScrollView, "scrollX", x);
        animator.setDuration(150);
        animator.start();

    }
}
