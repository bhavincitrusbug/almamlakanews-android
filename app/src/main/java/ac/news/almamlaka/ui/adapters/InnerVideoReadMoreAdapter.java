package ac.news.almamlaka.ui.adapters;

/**
 * Created by Mohammad
 * on 1/9/2018 9:39 AM.
 */

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.activities.VideoListActivity;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.TextViewCustom;

import static android.support.v4.content.ContextCompat.getColor;

public class InnerVideoReadMoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<Videos> mNews;
    private Context mContext;
    private VideoListActivity mVideoListActivity;
    private int selectedPosition;

    public InnerVideoReadMoreAdapter(Context context, ArrayList<Videos> results, VideoListActivity videoDetailsPlayerFragment) {
        this.mNews = results;
        this.mContext = context;
        this.mVideoListActivity = videoDetailsPlayerFragment;
        this.selectedPosition = 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        view = View.inflate(parent.getContext(), R.layout.item_list_video_sub, null);
        viewHolder = new NormalViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NormalViewHolder mNormalVH = (NormalViewHolder) holder;
        mNormalVH.mCat.setText(mNews.get(holder.getAdapterPosition()).getCategory_name());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mNormalVH.mTitle.setText(Html.fromHtml(mNews.get(holder.getAdapterPosition()).getTitle(), Html.FROM_HTML_MODE_LEGACY));
        } else
            mNormalVH.mTitle.setText(Html.fromHtml(mNews.get(holder.getAdapterPosition()).getTitle()));

        mNormalVH.mDate.setText((mNews.get(holder.getAdapterPosition()).getCreateDate()));
        mNormalVH.mView.setOnClickListener(v -> {
            mVideoListActivity.reloadVideoPlayer(mNews.get(holder.getAdapterPosition()).getLink());
            selectedPosition = holder.getAdapterPosition();
            this.notifyDataSetChanged();
        });
        mNormalVH.mView.setBackgroundColor(getColor(mContext, selectedPosition == holder.getAdapterPosition() ? R.color.dark_grey : R.color.black));

        Glide.with(mContext).load(Constants.IMAGE_PATH + mNews.get(holder.getAdapterPosition()).getThumb()).into(mNormalVH.mImageView);
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    class NormalViewHolder extends RecyclerView.ViewHolder {
        TextViewCustom mCat, mTitle, mDate;
        ImageView mImageView;
        View mView;

        NormalViewHolder(View itemView) {
            super(itemView);
            mView = itemView.findViewById(R.id.item_list_video_sub_layout);
            mCat = itemView.findViewById(R.id.item_list_video_sub_normal_cat);
            mTitle = itemView.findViewById(R.id.item_list_video_sub_normal_title);
            mDate = itemView.findViewById(R.id.item_list_video_sub_normal_time);
            mImageView = itemView.findViewById(R.id.item_list_video_sub_normal_image);
        }
    }
}

