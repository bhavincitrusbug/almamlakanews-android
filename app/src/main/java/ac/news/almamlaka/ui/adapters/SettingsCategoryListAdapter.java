package ac.news.almamlaka.ui.adapters;

/**
 * Created by Mohammad
 * on 1/9/2018 9:39 AM.
 */

import android.content.Context;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.NotificationCategory;
import ac.news.almamlaka.models.ResponseClass;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private DialogLoading mDialogLoading;
    private boolean mIsChecked = true;
    private ArrayList<NotificationCategory> notificationCategoryArrayList;
    private Context mContext;

    public SettingsCategoryListAdapter(Context context, ArrayList<NotificationCategory> results) {
        this.notificationCategoryArrayList = results;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        //        view = View.inflate(parent.getContext(), R.mView.item_list_settings_category, null);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_settings_category, parent, false);
        viewHolder = new NormalViewHolder(view);
        mDialogLoading = new DialogLoading(mContext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NormalViewHolder normalViewHolder = (NormalViewHolder) holder;
        NotificationCategory notificationCategory = notificationCategoryArrayList.get(holder.getAdapterPosition());
        normalViewHolder.name.setText(notificationCategory.getName());
        normalViewHolder.mSwitchCompat.setChecked(true);
        List disabledListIDs = new PrefManager().getDisabledNotificationsCategoryIDs(mContext);
        if (disabledListIDs != null && disabledListIDs.size() > 0) {
            for (int i = 0; i < disabledListIDs.size(); ++i) {
                if (disabledListIDs.get(i).equals(notificationCategory.getCat_id())) {
                    normalViewHolder.mSwitchCompat.setChecked(false);
                    break;
                }
            }
        }
        normalViewHolder.mSwitchCompat.setOnClickListener(v -> {
            mIsChecked = normalViewHolder.mSwitchCompat.isChecked();
            HandlerSwitch(mIsChecked, notificationCategory.getCat_id(), normalViewHolder);
        });
        // normalViewHolder.mSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> HandlerSwitch(isChecked, notificationCategory, normalViewHolder));
    }

    private void HandlerSwitch(boolean isChecked, String notificationCategory, NormalViewHolder normalViewHolder) {
        if (isChecked) {
            registerBtTopic(notificationCategory, normalViewHolder.mSwitchCompat);
        } else {
            unregisterBtTopic(notificationCategory, normalViewHolder.mSwitchCompat);
        }
    }

    private void registerBtTopic(String catID, SwitchCompat switchCompat) {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<ResponseClass> registerByCat = apiInterface.registerByCat(new PrefManager().getRegToken(mContext), Util.getUniquePhoneIdentity(), Constants.APP_ID,
                Constants.PLATFORM_ID, catID, Constants.AX_API_key, Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID));
        registerByCat.enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                ResponseClass body = response.body();
                if (body != null)
                    if (body.getStatus())
                        new PrefManager().deleteDisabledNotificationsCategoryIDs(mContext, catID);
                    else
                        switchCompat.setChecked(false);
                Log.d("register1", "onResponse: success" + response.code());
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                mDialogLoading.dismiss();
                Log.d("register1", "onFailure: success");

            }
        });
    }

    private void unregisterBtTopic(String catID, SwitchCompat switchCompat) {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<ResponseClass> mCallUnregisterByTopics = apiInterface.unregisterByCat(Constants.APP_ID, catID, Constants.PLATFORM_ID, Constants.AX_API_key, Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID));
        mCallUnregisterByTopics.enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                ResponseClass body = response.body();
                if (body != null)
                    if (body.getStatus()) {
                        new PrefManager().setDisabledNotificationsCategoryIDs(mContext, catID);
                    } else
                        switchCompat.setChecked(false);
                Log.d("register1", "onResponse: success" + response.code());
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                mDialogLoading.dismiss();
                Log.d("register1", "onFailure: success");
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationCategoryArrayList.size();
    }

    class NormalViewHolder extends RecyclerView.ViewHolder {
        TextViewCustom name;
        SwitchCompat mSwitchCompat;
        View layout;


        NormalViewHolder(View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.item_list_settings_category);
            name = itemView.findViewById(R.id.item_list_settings_tv_notification);
            mSwitchCompat = itemView.findViewById(R.id.item_list_settings_switch_notification);
        }
    }
}

