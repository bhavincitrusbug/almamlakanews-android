package ac.news.almamlaka.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.fragments.NewsDetailsFragment;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.FragmentCaller;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mohammad
 * on 1/3/2018 2:01 PM.
 */

public class DialogYesNo extends Dialog {

    private Context context;
    private String message;
    private int type;

    public DialogYesNo(@NonNull Context context, String message, int type) {
        super(context);
        this.message = message;
        this.context = context;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null)
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_warning);

        TextViewCustom yes_button = findViewById(R.id.dialog_warning_btn_ok);
        ProgressBar progress = findViewById(R.id.dialog_warning_progress_bar);
        TextViewCustom show_button = findViewById(R.id.dialog_warning_btn_show);
        TextViewCustom txtTitle = findViewById(R.id.dialog_warning_message);

        if (type == Constants.DIALOG_TYPE_SHOW_MSG_BTN) {
            show_button.setVisibility(View.VISIBLE);
            txtTitle.setText(Util.getMessage(message));
        } else {
            show_button.setVisibility(View.GONE);
            txtTitle.setText((message));
        }

        yes_button.setOnClickListener(v -> DialogYesNo.this.dismiss());
        show_button.setOnClickListener(v -> {
            show_button.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
            requestSingleNewsFromShare(Util.getScreenID(message));
        });
    }

    private void requestSingleNewsFromShare(String id) {
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<News> callLinkStream = apiInterface.getArticle(id);
        callLinkStream.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                News news = response.body();
                if (news != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.BUNDLE_ARTICLE, news);
                    new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) context, new NewsDetailsFragment(), bundle);
                }
                DialogYesNo.this.dismiss();
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                t.printStackTrace();
                DialogYesNo.this.dismiss();
            }
        });
    }
}