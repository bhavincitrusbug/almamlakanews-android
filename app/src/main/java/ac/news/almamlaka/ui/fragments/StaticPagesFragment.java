package ac.news.almamlaka.ui.fragments;

/*
 * Created by Mohammad
 * on 1/3/2018 2:45 PM.
 */

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.MenuItems;
import ac.news.almamlaka.models.StaticPages;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaticPagesFragment extends Fragment {

    View mView;
    MenuItems item;
    Call<StaticPages> mStaticPagesCall;
    DialogLoading mDialogLoading;
    WebView mDesc;
    private TextViewCustom mTitle, mDate;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_static_pages, container, false);
        new Util().freeMemory();
        initView();
        getHomeFeaturedNews(getCatID());
        return mView;
    }

    private void initView() {
        mTitle = mView.findViewById(R.id.static_pages_title);
        mDesc = mView.findViewById(R.id.static_pages_desc);
        mDate = mView.findViewById(R.id.static_pages_date);
    }

    private void getHomeFeaturedNews(int id) {
        if (getActivity() != null) {
            mDialogLoading = new DialogLoading(getActivity(), false);
            mDialogLoading.show();
        }
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        mStaticPagesCall = apiInterface.getLiveStreamLink(id);
        mStaticPagesCall.enqueue(new Callback<StaticPages>() {
            @Override
            public void onResponse(Call<StaticPages> call, Response<StaticPages> response) {
                mDialogLoading.dismiss();
                StaticPages staticPages = response.body();
                if (staticPages != null) {
                    mTitle.setText(staticPages.getTitle());
                    mDate.setText(staticPages.getCreated_on());
                    new Util().setDataInWebViewWithSize(mDesc, staticPages.getDescription(), new PrefManager().getFontSize(getActivity()));
                }
            }

            @Override
            public void onFailure(Call<StaticPages> call, Throwable t) {
                if (isAdded())
                    mDialogLoading.dismiss();
                t.printStackTrace();
            }
        });

    }

    private int getCatID() {
        if (getArguments() != null)
            item = (MenuItems) getArguments().getSerializable(Constants.STATIC_PAGE_ID);
        if (item != null) {
            return Integer.parseInt(item.getType_id());
        } else
            return 0;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mStaticPagesCall != null)
            mStaticPagesCall.cancel();
    }

}