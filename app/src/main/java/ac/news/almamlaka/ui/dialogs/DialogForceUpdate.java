package ac.news.almamlaka.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;

import ac.news.almamlaka.R;
import ac.news.almamlaka.utilities.TextViewCustom;


public class DialogForceUpdate extends Dialog {

    private Context context;
    private String message;

    public DialogForceUpdate(@NonNull Context context, String message) {
        super(context);
        this.message = message;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null)
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_update);
        this.setCancelable(false);
        TextViewCustom yes_button = findViewById(R.id.dialog_update_btn_ok);
        TextViewCustom txtTitle = findViewById(R.id.dialog_update_message);

        txtTitle.setText((message));


        yes_button.setOnClickListener(v -> {
                    openStore();
                    DialogForceUpdate.this.dismiss();
                }


        );

    }

    private void openStore() {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=ac.news.almamlaka")));
    }
}