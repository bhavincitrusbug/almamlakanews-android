package ac.news.almamlaka.ui.fragments;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.LiveStream;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.Util;
import ac.news.almamlaka.utilities.VideoControllerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mohammad
 * on 1/10/2018 8:18 AM.
 */

public class LiveFragment extends Fragment implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControl {

    final Handler handler = new Handler();
    SurfaceView mSurfaceView;
    MediaPlayer mPlayer;
    VideoControllerView mController;
    View mView;
    int mActionBar = 0;
    Call<LiveStream> mLiveStreamCall;
    TextViewCustom mTitle, mDesc;
    DialogLoading mDialogLoading;
    String mVideoLink = "";
    Runnable mRunnable;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_live, container, false);
        new Util().freeMemory();

        if (getArguments() != null)
            mActionBar = getArguments().getInt(Constants.ACTION_BAR);
        mDialogLoading = new DialogLoading(getActivity());
        mDialogLoading.show();

        mSurfaceView = mView.findViewById(R.id.videoSurface);
        mTitle = mView.findViewById(R.id.live_stream_title);
        mDesc = mView.findViewById(R.id.live_stream_desc);
        mDialogLoading = new DialogLoading(getActivity());
        SurfaceHolder videoHolder = mSurfaceView.getHolder();
        videoHolder.addCallback(this);

        mPlayer = new MediaPlayer();
        mController = new VideoControllerView(getContext());

        getStreamingLink();
        return mView;
    }

    private void getStreamingLink() {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        mLiveStreamCall = apiInterface.getLiveStreamLink();
        mLiveStreamCall.enqueue(new Callback<LiveStream>() {
            @Override
            public void onResponse(Call<LiveStream> call, Response<LiveStream> response) {
                LiveStream ls = response.body();
                if (ls != null && ls.getLivestream() != null) {
                    initialLink(ls);
                }
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<LiveStream> call, Throwable t) {
                t.printStackTrace();
                mDialogLoading.dismiss();

            }
        });
    }

    private void initialLink(LiveStream videoLink) {
        mVideoLink = videoLink.getLivestream();
        mDesc.setText(videoLink.getLivestream_description());
        mTitle.setText(videoLink.getLivestream_title());

        try {
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            if (getActivity() != null) {
                mPlayer.setDataSource(getActivity(), Uri.parse(videoLink.getLivestream()));
                mPlayer.setOnPreparedListener(this);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mView.setOnTouchListener((v, event) -> {
            mController.show();
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        new Util().showLiveIconActionBar((AppCompatActivity) getActivity(), false, mActionBar);
    }

    @Override
    public void onStop() {
        super.onStop();
        new Util().showLiveIconActionBar((AppCompatActivity) getActivity(), true, mActionBar);
        if (mLiveStreamCall != null)
            mLiveStreamCall.cancel();
        handler.removeCallbacks(mRunnable);
        mPlayer.stop();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mPlayer.setDisplay(holder);
        try {
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    if (mVideoLink.isEmpty()) {
                        handler.postDelayed(this, 500);
                    } else {
                        mPlayer.prepareAsync();
                        handler.removeCallbacks(this);
                    }
                }
            };
            handler.postDelayed(mRunnable, 500);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // End SurfaceHolder.Callback

    // Implement SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }
    // End MediaPlayer.OnPreparedListener

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    // Implement MediaPlayer.OnPreparedListener
    @Override
    public void onPrepared(MediaPlayer mp) {
        mDialogLoading.dismiss();
        mController.setMediaPlayer(this);
        mController.setAnchorView(mView.findViewById(R.id.videoSurfaceContainer));
        mPlayer.start();
        mController.show();
    }

    @Override
    public void start() {
        mPlayer.start();
    }

    @Override
    public void pause() {
        mPlayer.pause();
    }

    @Override
    public boolean isPlaying() {
        return mPlayer.isPlaying();
    }

    // Implement VideoMediaController.MediaPlayerControl
    @Override
    public boolean canPause() {
        return true;
    }
}
