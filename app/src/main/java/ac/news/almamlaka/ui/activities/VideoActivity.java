package ac.news.almamlaka.ui.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import ac.news.almamlaka.R;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;

public class VideoActivity extends AppCompatActivity {

    DialogLoading mDialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        VideoView videoView = findViewById(R.id.tutorial_activity_video_player);
        MediaController mc = new MediaController(this);
        videoView.setMediaController(mc);
        mDialogLoading = new DialogLoading(this);
        mDialogLoading.show();
        try {
            getSupportActionBar().hide();
            getActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getIntent().getStringExtra(Constants.ACTIVITY_LINKS) != null) {
            runOnUiThread(() -> {
                videoView.setVideoURI(Uri.parse(getIntent().getStringExtra(Constants.ACTIVITY_LINKS)));
                videoView.setOnPreparedListener(mp -> {
                    videoView.start();
                    mDialogLoading.dismiss();
                });
            });
        }
    }
}
