package ac.news.almamlaka.ui.adapters;

/**
 * Created by Mohammad
 * on 1/9/2018 9:39 AM.
 */

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.fragments.NewsDetailsFragment;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.FragmentCaller;
import ac.news.almamlaka.utilities.TextViewCustom;

public class ArticlesReadMoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<News> mNews;
    private Context mContext;

    public ArticlesReadMoreAdapter(Context context, ArrayList<News> results) {
        this.mNews = results;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        view = View.inflate(parent.getContext(), R.layout.item_list_article_more, null);
        viewHolder = new NormalViewHolder(view);

        view.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_ARTICLE, mNews.get(viewHolder.getAdapterPosition()));
            new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) mContext, new NewsDetailsFragment(), bundle);

        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NormalViewHolder normalViewHolder = (NormalViewHolder) holder;
        News news = mNews.get(holder.getAdapterPosition());
        normalViewHolder.mCat.setText(news.getKeywords());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            normalViewHolder.mTitle.setText(Html.fromHtml(news.getTitle(), Html.FROM_HTML_MODE_LEGACY));
        } else
            normalViewHolder.mTitle.setText(Html.fromHtml(news.getTitle()));

        normalViewHolder.mDate.setText((news.getStart_publish_date()));
        Glide.with(mContext).load(Constants.IMAGE_PATH + news.getMain_image()).into(normalViewHolder.mImageView);

    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    class NormalViewHolder extends RecyclerView.ViewHolder {
        TextViewCustom mCat, mTitle, mDate;
        ImageView mImageView;

        NormalViewHolder(View itemView) {
            super(itemView);
            mCat = itemView.findViewById(R.id.item_list_video_normal_cat);
            mTitle = itemView.findViewById(R.id.item_list_video_normal_title);
            mDate = itemView.findViewById(R.id.item_list_video_normal_time);
            mImageView = itemView.findViewById(R.id.item_list_video_normal_image);
        }
    }
}

