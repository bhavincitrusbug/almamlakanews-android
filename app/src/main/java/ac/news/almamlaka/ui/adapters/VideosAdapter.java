package ac.news.almamlaka.ui.adapters;

/**
 * Created by Mohammad
 * on 1/9/2018 9:39 AM.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.activities.VideoListActivity;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.TextViewCustom;

public class VideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<Videos> mResults;
    private Context mContext;
    private boolean hideFeatured;
    private FirebaseAnalytics mFirebaseAnalytics;

    public VideosAdapter(Context context, ArrayList<Videos> results, boolean hideFeatured) {
        this.mResults = results;
        this.mContext = context;
        this.hideFeatured = hideFeatured;
        callingAnalytics();
    }


    private void callingAnalytics() {
        if (hideFeatured) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Programs");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            mFirebaseAnalytics.setCurrentScreen((Activity) mContext, "Programs", "VideosSectionFragment");

        } else {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "DigitalVideos");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            mFirebaseAnalytics.setCurrentScreen((Activity) mContext, "DigitalVideos", "VideosSectionFragment");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case Constants.ITEM_FEATURED:
                view = View.inflate(parent.getContext(), R.layout.item_list_video_featured, null);
                viewHolder = new FeaturedViewHolder(view);
                break;
            case Constants.ITEM_NORMAL:
                view = View.inflate(parent.getContext(), R.layout.item_list_video_normal, null);
                viewHolder = new NormalViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        Videos videos = mResults.get(holder.getAdapterPosition());

        switch (viewType) {
            case Constants.ITEM_FEATURED:
                FeaturedViewHolder featuredViewHolder = (FeaturedViewHolder) holder;
                featuredViewHolder.mCat.setText(videos.getCategory_name());
                featuredViewHolder.mName.setText(videos.getTitle());
                Glide.with(mContext).load(Constants.IMAGE_PATH + videos.getThumb()).into(featuredViewHolder.mImageView);
                featuredViewHolder.layout.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, VideoListActivity.class);
                    intent.putExtra(Constants.VIDEO_TYPE_DETAILS_TAG, mResults.get(holder.getAdapterPosition()).getCategory_id());
                    mContext.startActivity(intent);

                });
                break;
            case Constants.ITEM_NORMAL:
                NormalViewHolder normalViewHolder = (NormalViewHolder) holder;
                normalViewHolder.mCat.setText(videos.getCategory_name());
                normalViewHolder.mName.setText(videos.getTitle());
                normalViewHolder.mDate.setText(videos.getCreateDate());
                Glide.with(mContext).load(Constants.IMAGE_PATH + videos.getThumb()).into(normalViewHolder.mImageView);
                normalViewHolder.layout.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, VideoListActivity.class);
                    intent.putExtra(Constants.VIDEO_TYPE_DETAILS_TAG, mResults.get(holder.getAdapterPosition()).getCategory_id());
                    mContext.startActivity(intent);
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {

        return (position == 0 && !hideFeatured) ? Constants.ITEM_FEATURED : Constants.ITEM_NORMAL;
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    class FeaturedViewHolder extends RecyclerView.ViewHolder {
        TextViewCustom mName, mCat, mVideosCount;
        ImageView mImageView;
        View layout;

        FeaturedViewHolder(View itemView) {
            super(itemView);
            mCat = itemView.findViewById(R.id.item_list_video_featured_cat);
            mName = itemView.findViewById(R.id.item_list_video_featured_name);
            mVideosCount = itemView.findViewById(R.id.item_list_video_featured_video_count);
            mImageView = itemView.findViewById(R.id.item_list_video_featured_image);
            layout = itemView.findViewById(R.id.item_list_video_featured_layout);
        }
    }

    class NormalViewHolder extends RecyclerView.ViewHolder {
        TextViewCustom mCat, mDate, mName;
        ImageView mImageView;
        View layout;

        NormalViewHolder(View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.item_list_video_normal_name);
            mCat = itemView.findViewById(R.id.item_list_video_normal_cat);
            layout = itemView.findViewById(R.id.item_list_video_normal_layout);
            mDate = itemView.findViewById(R.id.item_list_video_normal_time);
            mImageView = itemView.findViewById(R.id.item_list_video_normal_image);
        }
    }
}

