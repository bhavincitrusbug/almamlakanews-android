package ac.news.almamlaka.ui.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.adapters.ArticlePagerAdapter;
import ac.news.almamlaka.ui.adapters.ArticlesReadMoreAdapter;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.TextViewCustom;
import ac.news.almamlaka.utilities.TextViewCustomBold;
import ac.news.almamlaka.utilities.Util;
import ac.news.almamlaka.utilities.rtlViewPager.RtlViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mohammad
 * on 1/9/2018 11:20 AM.
 */

public class NewsDetailsFragment extends Fragment implements ViewPager.OnPageChangeListener {
    View mView;
    News mArticle;
    LinearLayout mPagerIndicator;
    int mDotsCount;
    ImageView[] mDots;
    ArticlePagerAdapter mAdapter;
    ProgressBar mProgressBar;
    ImageView mImageView;
    RelativeLayout mViewpagerHolder;
    FloatingActionMenu mFab;
    Call<ArrayList<News>> mCallGetNews;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_article, container, false);
        new Util().freeMemory();
        getArgs();
        return mView;
    }

    private void getArgs() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mArticle = (News) bundle.getSerializable(Constants.BUNDLE_ARTICLE);
        }
        if (mArticle != null)
            loadArticle(mArticle);
        else
            Util.showErrorView(getContext());
    }

    private void loadArticle(News article) {

        TextViewCustom mDate, mCat;
        TextViewCustomBold mTitle;
        WebView mDesc;
        mProgressBar = mView.findViewById(R.id.article_read_more_progress_bar);
        mImageView = mView.findViewById(R.id.article_header_image);
        mDate = mView.findViewById(R.id.article_date);
        mTitle = mView.findViewById(R.id.article_title);
        mCat = mView.findViewById(R.id.article_cat);
        mDesc = mView.findViewById(R.id.article_desc);
        mViewpagerHolder = mView.findViewById(R.id.article_slider);
        mFab = mView.findViewById(R.id.FloatingActionMenu1);
        loadFabMenu();

        mDate.setText((article.getStart_publish_date()));
        mTitle.setText(article.getTitle());
        mCat.setText(article.getKeywords());
        //        new Util().setDataInWebView(mDesc, article.getDescription());
        //mDesc.setText(article.getDescription());
        new Util().setDataInWebViewWithSize(mDesc, article.getDescription(), new PrefManager().getFontSize(getActivity()));

        loadSliderOrVideo(article);
        getReadMoreData(article.getId());
    }

    void loadFabMenu() {
        FloatingActionButton menu1 = mView.findViewById(R.id.subFloatingMenuShare);
        FloatingActionButton menu2 = mView.findViewById(R.id.subFloatingMenuBookMark);

        menu1.setOnClickListener(v -> new Util().Share(getContext(), mArticle.getNews_url()));
        menu2.setOnClickListener(v -> new Util().BookedMarkArticles(getContext(), mArticle));
    }

    private void loadSliderOrVideo(News article) {
        if (article.getImages() != null)
            if (article.getImages().size() == 0 && mArticle.getVideos().size() == 0) {
                mViewpagerHolder.setVisibility(View.GONE);
                mImageView.setVisibility(View.VISIBLE);
                if (getActivity() != null)
                    Glide.with(getActivity()).load(Constants.IMAGE_PATH + article.getMain_image()).into(mImageView);
            } else {
                mViewpagerHolder.setVisibility(View.VISIBLE);
                mImageView.setVisibility(View.GONE);
                loadTheSlider(article);
            }
    }

    private void getReadMoreData(String id) {
        mProgressBar.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        mCallGetNews = apiInterface.getArticlesSeeMore(id);
        mCallGetNews.enqueue(new Callback<ArrayList<News>>() {
            @Override
            public void onResponse(Call<ArrayList<News>> call, Response<ArrayList<News>> response) {
                mProgressBar.setVisibility(View.GONE);
                if (response.body() != null)
                    if (response.code() == 200)
                        loadAdapter(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<News>> call, Throwable t) {
                if (isAdded())
                    mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public void loadTheSlider(News article) {
        final RtlViewPager mViewPagerIntroImages = mView.findViewById(R.id.pager_article);
        mPagerIndicator = mView.findViewById(R.id.viewPagerCountDots);
        mAdapter = new ArticlePagerAdapter(getContext(), mImagesAndVideosArray(article));
        mViewPagerIntroImages.setAdapter(mAdapter);
        mViewPagerIntroImages.setCurrentItem(0);
        mViewPagerIntroImages.setOnPageChangeListener(this);
        setUiPageViewController();
    }

    private void loadAdapter(ArrayList<News> body) {
        RecyclerView recyclerView = mView.findViewById(R.id.article_recycler_view_read_more);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(new ArticlesReadMoreAdapter(getContext(), body));
    }

    private List<Videos> mImagesAndVideosArray(News leadStories) {

        ArrayList<Videos> mImagesAndVideosArray = new ArrayList<>();

        Videos i = new Videos();
        i.setLink("");
        i.setThumb(leadStories.getMain_image());
        mImagesAndVideosArray.add(i);


        mImagesAndVideosArray.addAll(leadStories.getVideos());
        return mImagesAndVideosArray;
    }

    private void setUiPageViewController() {
        mDotsCount = mAdapter.getCount();
        mDots = new ImageView[mDotsCount];
        for (int i = 0; i < mDotsCount; i++) {
            mDots[i] = new ImageView(getContext());
            mDots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected_item));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);
            mPagerIndicator.addView(mDots[i], params);
        }
        mDots[0].setImageDrawable(getResources().getDrawable(R.drawable.dot_selected_item));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mCallGetNews != null)
            mCallGetNews.cancel();

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < mDotsCount; i++) {
            mDots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected_item));
        }
        mDots[position].setImageDrawable(getResources().getDrawable(R.drawable.dot_selected_item));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
