package ac.news.almamlaka.ui.fragments;


import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.interfaces.CallbackRefreshVideo;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.adapters.InnerVideoReadMoreAdapter;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.CustomMediaController;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoDetailsPlayerFragment extends Fragment implements CallbackRefreshVideo {


    View mView;
    VideoView mVideoView;
    RecyclerView mRecyclerView;
    DialogLoading mDialogLoading;
    Call<ArrayList<Videos>> mCallVideos;
    ArrayList<Videos> mVideosArrayList;

    public VideoDetailsPlayerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_video_details_player, container, false);
        initView();
        getVideosList(getArgs());
        return mView;
    }

    private void initView() {

        CustomMediaController mediaController = new CustomMediaController(getActivity());

        mVideoView = mView.findViewById(R.id.fragment_video_details_videoView);
        mRecyclerView = mView.findViewById(R.id.fragment_video_details_recyclerView);

        mVideoView.setMediaController(mediaController);

        mVideosArrayList = new ArrayList<>();
        if (getActivity() != null) {
            mDialogLoading = new DialogLoading(getActivity());
            mDialogLoading.show();
            mediaController.setListener(() -> getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE));
        }
    }

    private void getVideosList(String id) {
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        mCallVideos = apiInterface.getVideosList(id, "1");

        mCallVideos.enqueue(new Callback<ArrayList<Videos>>() {
            @Override
            public void onResponse(Call<ArrayList<Videos>> call, Response<ArrayList<Videos>> response) {
                Log.d("pohnonoln", "" + response.code());
                mVideosArrayList = response.body();
                if (mVideosArrayList != null && mVideosArrayList.size() > 0) {
                    loadAdapter(mVideosArrayList);
                }
                mDialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<Videos>> call, Throwable t) {
                Log.d("", "" + t.getMessage());
                mDialogLoading.dismiss();
            }
        });

    }

    private String getArgs() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            return bundle.getString(Constants.VIDEO_TYPE_DETAILS_TAG, Constants.FEATURED);
        } else
            return Constants.FEATURED;


    }

    private void loadAdapter(ArrayList<Videos> body) {
        initVideoView(body.get(0).getLink());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(new InnerVideoReadMoreAdapter(getContext(), body, null));
    }

    private void initVideoView(String url) {

        mVideoView.setVideoURI(Uri.parse(url));
        mVideoView.setOnErrorListener((mp, what, extra) -> {
            mDialogLoading.dismiss();
            return false;
        });
        mVideoView.setOnPreparedListener(mp -> {
            mVideoView.start();
            mDialogLoading.dismiss();
        });


    }

    @Override
    public void reloadVideoPlayer(String url) {
        mDialogLoading.show();
        initVideoView(url);
    }
}
