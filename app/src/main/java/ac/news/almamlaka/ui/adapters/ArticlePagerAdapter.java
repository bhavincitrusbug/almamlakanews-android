package ac.news.almamlaka.ui.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.Util;

/*
 * Created by Mohammad
 * on 1/3/2018 2:46 PM.
 */
public class ArticlePagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<Videos> mVideos;

    public ArticlePagerAdapter(Context mContext, List<Videos> mVideos) {
        this.mContext = mContext;
        this.mVideos = mVideos;
    }

    @Override
    public int getCount() {
        return mVideos.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_pager_article, container, false);
        ImageView imageView = itemView.findViewById(R.id.item_pager_article_image);
        ImageView mVideoIcon = itemView.findViewById(R.id.item_pager_article_image_video_icon);

        Glide.with(mContext).load(Constants.IMAGE_PATH + mVideos.get(position).getThumb()).into(imageView);

        if (mVideos.get(position).getLink() != null && !mVideos.get(position).getLink().isEmpty()) {
            mVideoIcon.setVisibility(View.VISIBLE);
        } else
            mVideoIcon.setVisibility(View.GONE);

        itemView.setOnClickListener(v -> {
            if (mVideos.get(position).getLink() != null && !mVideos.get(position).getLink().isEmpty())
                Util.callVideoPlayer((AppCompatActivity) mContext, mVideos.get(position).getLink());
        });
        container.addView(itemView);
        return itemView;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
