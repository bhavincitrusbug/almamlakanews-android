package ac.news.almamlaka.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.models.Videos;
import ac.news.almamlaka.ui.adapters.VideosAdapter;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mohammad
 * on 1/9/2018 9:33 AM.
 */

public class VideosSectionFragment extends Fragment {
    View mView;
    RecyclerView mRecyclerView;
    boolean mHideFeatured;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static VideosSectionFragment newInstance(News news, boolean hideFeatured) {
        VideosSectionFragment fragmentFirst = new VideosSectionFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.videos_section_frag_news, news);
        args.putSerializable(Constants.videos_section_frag_video, news.getVideos());
        args.putBoolean(Constants.videos_section_frag_hide_Featured, hideFeatured);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_videos, container, false);
        initView();
        new Util().freeMemory();
        loadAdapter();
        return mView;
    }

    private void initView() {
        mRecyclerView = mView.findViewById(R.id.fragment_videos_recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void loadAdapter() {

        ArrayList<Videos> i = getArgs();
        if (i == null || i.size() == 0) {
            getHomeFeaturedNews();
        } else {
            mRecyclerView.setAdapter(new VideosAdapter(getContext(), i, mHideFeatured));
        }
    }

    private ArrayList<Videos> getArgs() {
        if (getArguments() != null)
            mHideFeatured = getArguments().getBoolean(Constants.videos_section_frag_hide_Featured);
        return (ArrayList<Videos>) getArguments().getSerializable(Constants.videos_section_frag_video);
    }

    private void getHomeFeaturedNews() {
        assert getActivity() != null;
        DialogLoading mDialogLoading = new DialogLoading(getActivity(), false);
        mDialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<ArrayList<News>> callGetNews = apiInterface.getNews();
        callGetNews.enqueue(new Callback<ArrayList<News>>() {
            @Override
            public void onResponse(Call<ArrayList<News>> call, Response<ArrayList<News>> response) {
                mDialogLoading.dismiss();
                ArrayList<News> body = response.body();
                if (getActivity() != null)
                    if (response.code() == 200)
                        if (body != null) {
                            for (News items : body)
                                if (items.isIs_featured_videos()) {
                                    mRecyclerView.setAdapter(new VideosAdapter(getContext(), items.getVideos(), mHideFeatured));// hai akeed programs
                                }


                        }
            }

            @Override
            public void onFailure(Call<ArrayList<News>> call, Throwable t) {
                mDialogLoading.dismiss();
                t.printStackTrace();
            }
        });

    }
}
