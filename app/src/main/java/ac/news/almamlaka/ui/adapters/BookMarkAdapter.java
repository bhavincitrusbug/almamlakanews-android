package ac.news.almamlaka.ui.adapters;

/**
 * Created by Mohammad
 * on 1/9/2018 9:39 AM.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.List;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.dialogs.DialogLoading;
import ac.news.almamlaka.ui.fragments.LeadStoryFragment;
import ac.news.almamlaka.ui.fragments.NewsDetailsFragment;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.FragmentCaller;
import ac.news.almamlaka.utilities.TextViewCustom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookMarkAdapter extends RecyclerView.Adapter<BookMarkAdapter.MyViewHolder> {
    private Context mContext;
    private List<News> mPageClasses;

    public BookMarkAdapter(Context context, List<News> cartList) {
        this.mContext = context;
        this.mPageClasses = cartList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_book_more, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // final News item = mPageClasses.get(position);
        holder.mCat.setText(mPageClasses.get(holder.getAdapterPosition()).getKeywords());
        holder.mTitle.setText(mPageClasses.get(holder.getAdapterPosition()).getTitle());
        holder.mTime.setText(mPageClasses.get(holder.getAdapterPosition()).getStart_publish_date());

        Glide.with(mContext).load(mPageClasses.get(holder.getAdapterPosition()).getMain_image()).into(holder.thumbnail);

        holder.layout.setOnClickListener(v -> {
            getBookedNews(mPageClasses.get(holder.getAdapterPosition()).getId());
            //Bundle bundle = new Bundle();
            //bundle.putSerializable(Constants.BUNDLE_ARTICLE, mPageClasses.get(holder.getAdapterPosition()));
            //new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) mContext, new NewsDetailsFragment(), bundle);
        });


    }

    private void getBookedNews(String id) {
        DialogLoading dialogLoading = new DialogLoading(mContext);
        dialogLoading.show();
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<News> call = apiInterface.getArticle(id);
        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                dialogLoading.dismiss();
                News news = response.body();
                if (news != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.BUNDLE_ARTICLE, news);
                    if (news.isIs_lead())
                        new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) mContext, new LeadStoryFragment(), bundle);
                    else
                        new FragmentCaller().fragmentAdderWithBundle((AppCompatActivity) mContext, new NewsDetailsFragment(), bundle);
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                if (mContext != null) {
                    dialogLoading.dismiss();
                    t.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPageClasses.size();
    }

    public void removeItem(int position) {
        mPageClasses.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(News item, int position) {
        mPageClasses.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextViewCustom mCat, mTitle, mTime;
        public ImageView thumbnail;
        public RelativeLayout viewBackground, viewForeground;
        public View layout;

        MyViewHolder(View view) {
            super(view);
            mCat = view.findViewById(R.id.item_list_book_mark_cat);
            mTitle = view.findViewById(R.id.item_list_book_mark_title);
            mTime = view.findViewById(R.id.item_list_book_mark_time);
            thumbnail = view.findViewById(R.id.item_list_book_mark_image);
            layout = view.findViewById(R.id.item_list_book_mark_view);

            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }
}
//        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//    private final ArrayList<News> mVideos;
//    private Context mContext;
//
//    public BookMarkAdapter(Context mContext, ArrayList<News> results) {
//        this.mVideos = results;
//        this.mContext = mContext;
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view;
//        RecyclerView.ViewHolder viewHolder;
//        view = View.inflate(parent.getContext(), R.mView.item_list_book_more, null);
//        viewHolder = new NormalViewHolder(view);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        NormalViewHolder normalViewHolder = (NormalViewHolder) holder;
//        normalViewHolder.mCat.setText("NormalViewHolder" + holder.getAdapterPosition());
//        normalViewHolder.mTitle.setText("here is the mTitle from server");
//        normalViewHolder.mDate.setText("Tuesday, January 9, 2018");
//        Glide.with(mContext).load("https://static.wixstatic.com/media/ce888e_593d80b58e04450b85b457c438d305c6.jpg_srz_980_649_85_22_0.50_1.20_0.00_jpg_srz").into(normalViewHolder.mImageView);
//    }
//
//    @Override
//    public int getItemCount() {
//        return mVideos.size();
//    }
//
//    class NormalViewHolder extends RecyclerView.ViewHolder {
//        TextViewCustom mCat, mTitle, mDate;
//        ImageView mImageView;
//
//        NormalViewHolder(View itemView) {
//            super(itemView);
//            mCat = itemView.findViewById(R.id.item_list_book_mark_cat);
//            mTitle = itemView.findViewById(R.id.item_list_book_mark_desc);
//            mDate = itemView.findViewById(R.id.item_list_book_mark_time);
//            mImageView = itemView.findViewById(R.id.item_list_book_mark_image);
//        }
//    }
//}
//
