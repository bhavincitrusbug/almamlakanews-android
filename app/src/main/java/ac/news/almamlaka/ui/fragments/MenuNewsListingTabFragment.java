package ac.news.almamlaka.ui.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.MenuItems;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.TextViewCustom;

/**
 * Created by Mohammad
 * on 1/10/2018 8:18 AM.
 */

public class MenuNewsListingTabFragment extends Fragment {

    View mView;
    ViewPager mViewPager;
    TabLayout mTabLayout;
    MenuItems mMenuItems;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_news_listing_tab, container, false);
        mViewPager = mView.findViewById(R.id.viewpager);
        mTabLayout = mView.findViewById(R.id.tabs);

        setupViewPager();
        changeTabsFont(mTabLayout);
        return mView;
    }

    private void setupViewPager() {
        if (getActivity() != null) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
            if (getCatID().get_children().size() != 0) {
                for (MenuItems items : getCatID().get_children()) {

                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.CAT_ID_NEWS, items);
                    MenuNewsListingFragment frg = new MenuNewsListingFragment();
                    frg.setArguments(bundle);
                    adapter.addFragment(frg, items);
                }
            } else {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.CAT_ID_NEWS, getCatID());
                MenuNewsListingFragment frg = new MenuNewsListingFragment();
                frg.setArguments(bundle);
                adapter.addFragment(frg, getCatID());
            }

            mViewPager.setAdapter(adapter);
        }
    }

    private void changeTabsFont(TabLayout tabLayout) {
        if (getContext() == null)
            return;
        tabLayout.setupWithViewPager(mViewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextViewCustom tv = (TextViewCustom) LayoutInflater.from(getActivity()).inflate(R.layout.item_tab_custom_title, null);
            tv.setTypeface(Typeface.createFromAsset(getContext().getAssets(), Constants.FONT));
            tabLayout.getTabAt(i).setCustomView(tv);
        }
    }

    private MenuItems getCatID() {
        if (getArguments() != null)
            mMenuItems = (MenuItems) getArguments().getSerializable(Constants.CAT_ID_NEWS);
        return mMenuItems;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        void addFragment(Fragment fragment, MenuItems title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title.getName_ar());
        }
    }
}