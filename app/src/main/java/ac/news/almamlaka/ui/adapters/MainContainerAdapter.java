package ac.news.almamlaka.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.fragments.HomePagerFragment;
import ac.news.almamlaka.ui.fragments.VideosSectionFragment;

/**
 * Created by Mohammad
 * on 2/12/2018 3:03 PM.
 */

public class MainContainerAdapter extends FragmentPagerAdapter {

    private ArrayList<News> body;

    public MainContainerAdapter(FragmentManager fm, ArrayList<News> body) {
        super(fm);
        this.body = body;
    }

    @Override
    public Fragment getItem(int position) {
        if (body.get(position).isIs_featured_videos())
            return VideosSectionFragment.newInstance(body.get(position), false);
        else {
            return HomePagerFragment.newInstance(body.get(position), position);
        }
    }

    @Override
    public int getCount() {
        return body.size();
    }
}
