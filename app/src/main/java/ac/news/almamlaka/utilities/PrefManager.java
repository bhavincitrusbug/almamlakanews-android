package ac.news.almamlaka.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.dialogs.DialogYesNo;

/**
 * Created by Mohammad
 * on 1/7/2018 10:42 AM.
 */

public class PrefManager {
    private SharedPreferences prefs;

    public String getLanguageForDevice(Context a) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        Log.e("getLanguageForDevice: ", prefs.getString(Constants.LANGUAGE, Constants.AR));
        return prefs.getString(Constants.LANGUAGE, Constants.AR);
    }

    void setLanguageForDevice(Context a, String lang) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.LANGUAGE, lang);
        editor.apply();
    }

    void setBookMarkedItem(Context mContext, News results) {
        try {
            ArrayList<News> oldResult = getBookMarkedItem(mContext);

            if (oldResult != null && oldResult.size() > 0) {
                for (int i = 0; i < oldResult.size(); ++i) {
                    if (oldResult.get(i).getId().equals(results.getId())) {
                        new DialogYesNo(mContext, mContext.getString(R.string.book_mark_exist), 0).show();
                        return;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            SharedPreferences.Editor prefsEditor = prefs.edit();
            ArrayList<News> arrayList = getBookMarkedItem(mContext);
            ArrayList<News> temp = new ArrayList<>();

            temp.addAll(arrayList);
            temp.add(results);

            Gson gson = new Gson();
            String json = gson.toJson(temp);
            prefsEditor.putString(Constants.HOME_PAGE_CLASS, json.replace("{},", ""));
            prefsEditor.apply();
            new DialogYesNo(mContext, mContext.getString(R.string.book_mark_added_success), 0).show();
        } catch (Exception e) {
            e.printStackTrace();
            new DialogYesNo(mContext, mContext.getString(R.string.book_mark_added_faild), 0).show();
        }
    }

    public ArrayList<News> getBookMarkedItem(Context mContext) {
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        Gson gson = new Gson();
        String json = prefs.getString(Constants.HOME_PAGE_CLASS, "");
        Type type = new TypeToken<ArrayList<News>>() {
        }.getType();

        if (json.length() < 1) {
            return new ArrayList<>();
        } else {
            return gson.fromJson(json, type);
        }
    }

    public void deleteBookMarkedItem(Context mContext, News deletedItem) {
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();

        ArrayList<News> item = getBookMarkedItem(mContext);
        for (News n : item) {
            if (n.getId().equals(deletedItem.getId())) {
                item.remove(n);
                break;
            }
        }
        prefsEditor.putString(Constants.HOME_PAGE_CLASS, gson.toJson(item));
        prefsEditor.apply();
    }

    public void deleteDisabledNotificationsCategoryIDs(Context mContext, String deletedItem) {
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();

        ArrayList<String> item = getDisabledNotificationsCategoryIDs(mContext);
        for (String n : item) {
            if (n.equals(deletedItem)) {
                item.remove(n);
                break;
            }
        }
        prefsEditor.putString(Constants.DISABLED_CATEGORY_ID_LIST_TAG, gson.toJson(item));
        prefsEditor.apply();
    }

    public ArrayList<String> getDisabledNotificationsCategoryIDs(Context mContext) {
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        Gson gson = new Gson();
        String json = prefs.getString(Constants.DISABLED_CATEGORY_ID_LIST_TAG, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();

        if (json == null || json.length() < 1) {
            return new ArrayList<>();
        } else {
            return gson.fromJson(json, type);
        }
    }

    public void setDisabledNotificationsCategoryIDs(Context mContext, String results) {
        try {
            ArrayList<String> oldResult = getDisabledNotificationsCategoryIDs(mContext);

            if (oldResult != null && oldResult.size() > 0) {
                for (int i = 0; i < oldResult.size(); ++i) {
                    if (oldResult.get(i).equals(results)) {
                        return;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            SharedPreferences.Editor prefsEditor = prefs.edit();
            ArrayList<String> arrayList = getDisabledNotificationsCategoryIDs(mContext);
            ArrayList<String> temp = new ArrayList<>();

            temp.addAll(arrayList);
            temp.add(results);

            Gson gson = new Gson();
            String json = gson.toJson(temp);
            prefsEditor.putString(Constants.DISABLED_CATEGORY_ID_LIST_TAG, json.replace("{},", ""));
            prefsEditor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isNotificationsEnable(Context a) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        return prefs.getBoolean(Constants.IS_NOTIFICATIONS_ENABLE, true);
    }


    public boolean isUrgentNotificationsEnable(Context a) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        return prefs.getBoolean(Constants.IS_URGENT_NOTIFICATIONS_ENABLE, true);
    }

    public boolean isLiveStreamNotificationsEnable(Context a) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        return prefs.getBoolean(Constants.IS_LIVE_STREAM_NOTIFICATIONS_ENABLE, true);
    }


    public void setIsLiveStreamNotificationsEnable(Context a, boolean isNotificationsEnable) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constants.IS_LIVE_STREAM_NOTIFICATIONS_ENABLE, isNotificationsEnable);
        editor.apply();
    }

    public void setIsUrgentNotificationsEnable(Context a, boolean isNotificationsEnable) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constants.IS_URGENT_NOTIFICATIONS_ENABLE, isNotificationsEnable);
        editor.apply();
    }


    public void setIsNotificationsEnable(Context a, boolean isNotificationsEnable) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constants.IS_NOTIFICATIONS_ENABLE, isNotificationsEnable);
        editor.apply();
    }

    public String getRegToken(Context a) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        return prefs.getString(Constants.GOOGLE_REG_TOKEN, "");
    }

    public void setRegToken(Context a, String token) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.GOOGLE_REG_TOKEN, token);
        editor.apply();
    }

    public void setFontSize(Context a, int size) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Constants.FONT_SIZE_SCALE_TAG, size);
        editor.apply();
    }

    public int getFontSize(Context a) {
        prefs = PreferenceManager.getDefaultSharedPreferences(a);
        return prefs.getInt(Constants.FONT_SIZE_SCALE_TAG, 0);
    }

    /*
    in order to delete all categories in shared pref if user disabled all notifications at once
     */
    public void deleteAllDisabledNotifications(Context mContext) {
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        ArrayList<String> item = getDisabledNotificationsCategoryIDs(mContext);
        prefsEditor.putString(Constants.DISABLED_CATEGORY_ID_LIST_TAG, gson.toJson(item));
        prefsEditor.apply();
    }
    //
    //    public void setBodyMessageForNotification(Context a, String body) {
    //        prefs = PreferenceManager.getDefaultSharedPreferences(a);
    //        SharedPreferences.Editor editor = prefs.edit();
    //        editor.putString(Constants.BASE_URL, body);
    //        editor.apply();
    //    }
    //
    //    public String getBodyMessageForNotification(Context a) {
    //        prefs = PreferenceManager.getDefaultSharedPreferences(a);
    //        return prefs.getString(Constants.BASE_URL, "");
    //    }
}

