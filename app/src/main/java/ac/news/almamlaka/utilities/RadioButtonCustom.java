package ac.news.almamlaka.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by alialquraan on 1/22/18.
 */

public class RadioButtonCustom extends android.support.v7.widget.AppCompatRadioButton {


    Typeface normal_ar = Typeface.createFromAsset(getContext().getAssets(), Constants.FONT);

    public RadioButtonCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RadioButtonCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadioButtonCustom(Context context) {
        super(context);
        init();

    }

    public void init() {
        super.setTypeface(normal_ar);
        if (this.getContext() != null) {
            this.setTextSize(getMyTextSize());
        }

    }

    float getMyTextSize() {
        return this.getTextSize() / getResources().getDisplayMetrics().scaledDensity + new PrefManager().getFontSize(this.getContext());
    }

}
