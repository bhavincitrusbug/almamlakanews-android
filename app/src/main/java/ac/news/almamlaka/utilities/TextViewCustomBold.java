package ac.news.almamlaka.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Mohammad on 9/19/2017 11:15 AM.
 */

public class TextViewCustomBold extends AppCompatTextView {


     Typeface bold_ar = Typeface.createFromAsset(getContext().getAssets(), "bold.ttf");

    public TextViewCustomBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TextViewCustomBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewCustomBold(Context context) {
        super(context);
        init();

    }

    public void init() {
        super.setTypeface(bold_ar);
        if (this.getContext() != null) {
            this.setTextSize(getMyTextSize());
        }

    }

    float getMyTextSize() {
        return this.getTextSize() / getResources().getDisplayMetrics().scaledDensity + new PrefManager().getFontSize(this.getContext());
    }

}
