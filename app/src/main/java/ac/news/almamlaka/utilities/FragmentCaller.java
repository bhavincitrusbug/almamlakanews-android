package ac.news.almamlaka.utilities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ac.news.almamlaka.R;
import ac.news.almamlaka.ui.fragments.HomePagerFragment;


/**
 * Created by Mohammad
 * on 1/7/2018 10:29 AM.
 */


public class FragmentCaller {

    public void callAnimation(Context a, Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = ((AppCompatActivity) a).getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_up, 0);
            ft.add(R.id.container_body, fragment);
            ft.addToBackStack("backStack");
            ft.commit();
        }
    }

    public void callReplacer(Context a, Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = ((AppCompatActivity) a).getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container_body, fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }

    public void callRemoveAllThenReplace(Context a, Fragment fragment) {
        try {
            if (fragment != null) {
                FragmentManager fragmentManager = ((AppCompatActivity) a).getSupportFragmentManager();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.container_body, fragment);
                ft.addToBackStack(null);
                try {
                    ft.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    ft.commitAllowingStateLoss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ReloadCurrentFragment(Context a, Fragment frg) {
        FragmentManager fragmentManager = ((AppCompatActivity) a).getSupportFragmentManager();
        // final FragmentTransaction ft = fragmentManager.beginTransaction();
        // ft.replace(R.id.container_body, fragment);
        // ft.commit();
        final FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.detach(frg);
        ft.attach(frg);
        ft.commit();

    }

    public void fragmentAdderWithBundle(AppCompatActivity a, Fragment fragment, Bundle bundle) {
        new Util().freeMemory();
        Util.hideKeyboard(a);
        if (fragment != null) {
            fragment.setArguments(bundle);

            FragmentManager fragmentManager = a.getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.add(R.id.container_body, fragment);
            ft.addToBackStack(null);// don't remove this line, this will reflect on changing the titles on back press
            try {
                ft.commit();
            } catch (Exception e) {
                ft.commitAllowingStateLoss();
            }
        }
    }

    public void fragmentReplacerWithBundle(AppCompatActivity a, Fragment fragment, Bundle bundle) {
        new Util().freeMemory();
        Util.hideKeyboard(a);
        if (fragment != null) {
            fragment.setArguments(bundle);

            FragmentManager fragmentManager = a.getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container_body, fragment);
            ft.addToBackStack(null);// don't remove this line, this will reflect on changing the titles on back press
            try {
                ft.commit();
            } catch (Exception e) {
                ft.commitAllowingStateLoss();
            }
        }
    }

    public void fragmentRemoveAllThenReplaceWithBundle(AppCompatActivity a, Fragment fragment, Bundle bundle) {
        new Util().freeMemory();
        Util.hideKeyboard(a);
        if (fragment != null) {
            fragment.setArguments(bundle);

            FragmentManager fragmentManager = a.getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            callAdd(a, new HomePagerFragment());
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container_body, fragment);
            ft.addToBackStack(null);// don't remove this line, this will reflect on changing the titles on back press
            try {
                ft.commit();
            } catch (Exception e) {
                ft.commitAllowingStateLoss();
            }
        }
    }

    public void callAdd(AppCompatActivity a, Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = a.getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.add(R.id.container_body, fragment);
            ft.addToBackStack("backStack");
            ft.commit();
        }
    }
}
