package ac.news.almamlaka.utilities;

/**
 * Created by Mohammad
 * on 1/7/2018 10:34 AM.
 */

public class Constants {

    public static final int _DIALOG_EXIT_APP = -1;
    public static final String IMAGE_PATH = "";
    public static final int ITEM_NORMAL = 1;
    public static final int ITEM_FEATURED = 0;
    public static final int ITEM_LOADING = 2;

    public static final String MENU_TYPE_CAT = "category";
    public static final String MENU_TYPE_VIDEO = "video";
    public static final String MENU_TYPE_API = "api";
    public static final String MENU_TYPE_PAGE = "page";
    public static final String MENU_TYPE_WEATHER = "weather";
    public static final String MENU_TYPE_BOOKMARK = "bookmark";
    public static final String MENU_TYPE_SETTINGS = "settings";

    public static final String IS_NOTIFICATIONS_ENABLE = "is_notifications_enable";
    public static final int DIALOG_TYPE_SHOW_MSG_BTN = 1;
    public static final int DIALOG_TYPE = 0;
    public static final String APP_VERSION = "2.1";
    public static final String IS_URGENT_NOTIFICATIONS_ENABLE = "is_urgent_notifications_enable";
    public static final String IS_LIVE_STREAM_NOTIFICATIONS_ENABLE = "is_live_stream_notifications_enable";

    //    public static String BASE_URL = "https://www.almamlakatv.com/";
//     public static String BASE_URL = "https://almamlkaportal-stg.arabiacell.net/";
    public static String BASE_URL = "https://almamlkaportal.arabiacell.net";
    // public static String BASE_URL = "http://almamlkatest.arabiacell.biz";
    public static String BASE_IMAGE_VIDEO_THUMB = "";
    public static String EN = "en";
    public static String AR = "ar";
    public static String LANGUAGE = "language";
    public static String BUNDLE_ARTICLE = "bundle_article";
    // public static String VIDEO_TEST_LINK = "http://royatv-vod.ercdn.net/hls/Z/bz/marwan-juma/marwan-juma.hb.smil/playlist.m3u8";
    public static String HOME_PAGE_CLASS = "HOME_PAGE_CLASS";
    public static String ACTION_BAR = "ACTION_BAR";
    public static String PK = "U0VkEbsL5WFpXdTcIoZK1jQeTWPmcdf5";//"sK8DkvuyKGeb19b437g4Cv33GXV49c9Q";
    public static String USERNAME = "AlmamlkaAdmin!@#123";
    public static String PASSWORD = "5u?5ST(7(Rd4?C";
    public static String ADMIN = "admin";
    public static int PAGE_SIZE = 15;
    public static String CAT_ID_NEWS = "CAT_ID_NEWS";
    public static String SOCIAL_FACEBOOK = "396902253989412";
    public static String SOCIAL_TWITTER = "AlMamlakaTV";
    public static String SOCIAL_YOUTUBE = "UC0jiFAzTgl17k7awGbuoYew";//"almamlakatv";
    public static String SOCIAL_INSTAGRAM = "almamlakatv";
    public static String FONT = "medium.ttf";
    public static String STATIC_PAGE_ID = "STATIC_PAGE_ID";
    public static String GOOGLE_REG_TOKEN = "google_reg_token";

    public static String APP_ID = "39";
    public static String AX_API_key = "dZ7z86YacJKF57fMKff7WpnV5MxJPcB43hmJgSLy";

    public static String FONT_SIZE_SCALE_TAG = "font_size_scale_tag";
    public static int FONT_SIZE_SMALL = -5;
    public static int FONT_SIZE_MEDIUM = 0;
    public static int FONT_SIZE_LARGE = 5;

    public static String ACTIVITY_LINKS = "activity_links";
    public static String NOTIFICATIONS_SCREEN_ID = "NOTIFICATIONS_SCREEN_ID";
    public static String NOTIFICATIONS_MESSAGE_BODY = "NOTIFICATIONS_MESSAGE_BODY";
    public static String HOME_PAGER_ITEM = "home_pager_item";
    public static String HOME_PAGER_ITEM_POS = "home_pager_item_pos";
    public static String FEATURED = "featured";
    public static String videos_section_frag_news = "videos_section_frag_news";
    public static String videos_section_frag_video = "videos_section_frag_video";
    public static String VIDEO_TYPE_DETAILS_TAG = "video_type_details_tag";
    public static String VIDEO_TYPE_FEATURED = "featured";
    public static String videos_section_frag_hide_Featured = "videos_section_frag_hide_featured";


    public static String DISABLED_CATEGORY_ID_LIST_TAG = "disabled_category_id_list_tag";

    public static String RESIDE_MENU = "reside_menu";
    public static String PLATFORM_ID = "1";
    public static long TIME_OUT = 2;

    public interface ACTION {

        String MAIN_ACTION = "com.marothiatechs.customnotification.action.main";

        String PREV_ACTION = "com.marothiatechs.customnotification.action.prev";
        String PLAY_ACTION = "com.marothiatechs.customnotification.action.play";
        String NEXT_ACTION = "com.marothiatechs.customnotification.action.next";
        String STARTFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.startforeground";
        String STOPFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.stopforeground";

    }
}
