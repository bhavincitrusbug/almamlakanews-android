package ac.news.almamlaka.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.LocaleList;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.choota.dev.ctimeago.TimeAgo;
import com.facebook.rebound.SpringSystem;
import com.tumblr.backboard.Actor;
import com.tumblr.backboard.imitator.ToggleImitator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.ui.activities.MainActivity;
import ac.news.almamlaka.ui.activities.VideoActivity;

/**
 * Created by Mohammad
 * on 1/7/2018 9:39 AM.
 */

public class Util {
    public static String formattedTime(long hours, long mins) {
        String minStr = "";
        String secStr = "";
        String Am = " AM";
        if (hours > 12) {
            Am = " PM";
            hours -= 12;
        }
        if (hours < 10) {
            minStr = "0";
        }
        if (mins < 10) {
            secStr = "0";
        }
        minStr += hours;
        secStr += mins;
        return minStr + ":" + secStr + Am;
    }

    public static String formattedTime24(long hours, long mins) {
        String minStr = "";
        String secStr = "";

        if (hours < 10) {
            minStr = "0";
        }
        if (mins < 10) {
            secStr = "0";
        }
        minStr += hours;
        secStr += mins;
        return minStr + ":" + secStr;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.showSoftInput(view, 0);
    }

    public static void PLEASE_CHECK_INTERNET(View view, final Activity activity) {

        Snackbar snackbar = Snackbar.make(view, activity.getString(R.string.msg_interntConection), 4000);
        View view1 = snackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view1.getLayoutParams();
        params.gravity = Gravity.TOP;
        view1.setLayoutParams(params);
        view1.setBackgroundColor(Color.RED);
        snackbar.show();

    }

    public static String GetCurrentDayArabic(int index) {
        switch (index) {
            case 0:
                return "السبت";
            case 1:
                return "الاحد";
            case 2:
                return "الاثنين";
            case 3:
                return "الثلاثاء";
            case 4:
                return "الاربعاء";
            case 5:
                return "الخميس";
            case 6:
                return "الجمعة";
        }


        return "";
    }

    public static String GetCurrentDayEnglish(int index) {
        switch (index) {
            case 0:
                return "Saturday";
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuseday";
            case 4:
                return "Winsday";
            case 5:
                return "thursday";
            case 6:
                return "friday";
        }


        return "";
    }

    public static void callPhoneCaller(AppCompatActivity a, String num) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + num));
        a.startActivity(callIntent);
    }

    public static void callUrl(AppCompatActivity a, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        a.startActivity(i);
    }

    public static void showErrorView(Context context) {

    }

    public static String timeAgoValidator(String dateTime) {
        String oldFormat = "yyyy-MM-dd hh:mm:ss";
        Date dt = null;

        if (dateTime != null) {
            try {
                DateFormat df = new SimpleDateFormat(oldFormat, Locale.US);
                df.setTimeZone(TimeZone.getTimeZone("JO"));
                dt = new java.sql.Date(df.parse(dateTime).getTime());
            } catch (ParseException pe) {
                pe.printStackTrace();
            }
        }
        return new TimeAgo().getTimeAgo(dt);
    }

    public static void addOnClickAnimation(View view) {
        new Actor.Builder(SpringSystem.create(), view).addMotion(new ToggleImitator(null, 1.0, 0.9), View.SCALE_X, View.SCALE_Y).build();
    }

    public static String getUniquePhoneIdentity() {
        String m_szDevIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10) + (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);

        String serial = null;
        try {
            serial = android.os.Build.class.getField("SERIAL").get(null).toString();

            // Go ahead and return the serial for api => 9
            return "android-" + new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            // String needs to be initialized
            serial = "serial"; // some value
        }
        return "android-" + new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }

    public static void callVideoPlayer(AppCompatActivity appCompatActivity, String Link) {
        try {
            MainActivity.callStopAllMedia();
            Intent intent;
            if (Link.contains("youtube")) {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Link));
                intent.setPackage("com.google.android.youtube");
            } else {
                intent = new Intent(appCompatActivity, VideoActivity.class);
                intent.putExtra(Constants.ACTIVITY_LINKS, Link);
            }
            appCompatActivity.startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean userHasInternetConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (connectivityManager != null) {
            netInfo = connectivityManager.getActiveNetworkInfo();
        }
        return (netInfo != null && netInfo.isConnected());
    }

    public static String getScreenID(String messageBody) {
        if (messageBody.contains("#")) {
            String[] splitArray = messageBody.split("#");
            return splitArray[1];
        } else {
            return "-1";
        }
    }

    public static String getMessage(String messageBody) {
        if (messageBody.contains("#")) {
            String[] splitArray = messageBody.split("#");
            return splitArray[0];
        } else {
            return messageBody;
        }
    }

    public void setDataInWebViewWithSize(WebView mWebView, String description, int fontSize) {
        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/medium.ttf\")}body {font-family: MyFont;font-size: medium;text-align: right;}</style>" + "</head><body style='text-align: right'>";
        String pas = "</body></html>";
        String myHtmlString = pish + description + pas;


        mWebView.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setDefaultFontSize(webSettings.getDefaultFontSize() + fontSize);

        ////Fit Web view content to screen
        //mWebView.setInitialScale(1);
        //mWebView.getSettings().setUseWideViewPort(true);
        //mWebView.getSettings().setLoadWithOverviewMode(true);
        //// If you want to support zoom buttons you can add this –
        //mWebView.getSettings().setBuiltInZoomControls(true);
        //mWebView.getSettings().setSupportZoom(true);

    }

    public void freeMemory() {
        try {
            System.runFinalization();
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLocale(String localeCode, AppCompatActivity appCompatActivity) {
        new PrefManager().setLanguageForDevice(appCompatActivity, localeCode);
        Locale locale = new Locale(localeCode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocales(new LocaleList(locale));
        } else {
            config.locale = locale;
        }

        appCompatActivity.getBaseContext().getResources().updateConfiguration(config, appCompatActivity.getBaseContext().getResources().getDisplayMetrics());
    }

    public void Share(Context _context, String text_to_share) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text_to_share);
        sendIntent.setType("text/plain");
        _context.startActivity(sendIntent);
    }

    public void showLiveIconActionBar(AppCompatActivity a, boolean showIcon, Integer actionBar) {
        if (actionBar != null)
            a.findViewById(actionBar).setVisibility(showIcon ? View.VISIBLE : View.GONE);
    }

    public void BookedMarkArticles(Context context, News homePageClass) {
        // TODO: 1/11/2018 change teh type to default
        new PrefManager().setBookMarkedItem(context, homePageClass);
    }


    //    DialogLoading dialogLoading;

    //    public void showLoadingDialog(Context context) {
    //
    //        if (dialogLoading != null && !dialogLoading.isShowing()) {
    //
    //        } else {
    //            dialogLoading = new DialogLoading(context);
    //            dialogLoading.show();
    //        }
    //
    //    }
    //
    //    public void dismissLoadingDialog() {
    //        if (dialogLoading != null && dialogLoading.isShowing()) {
    //            dialogLoading.dismiss();
    //        }
    //
    //    }

}
