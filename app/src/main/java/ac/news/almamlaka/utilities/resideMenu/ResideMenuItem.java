package ac.news.almamlaka.utilities.resideMenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import ac.news.almamlaka.R;
import ac.news.almamlaka.models.MenuItems;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.TextViewCustom;

/**
 * User: special
 * Date: 13-12-10
 * Time: 下午11:05
 * Mail: specialcyci@gmail.com
 */
public class ResideMenuItem extends LinearLayout {
    private ImageView iv_icon;
    private TextViewCustom tv_title;

    public ResideMenuItem(Context context, int icon, int title) {
        super(context);
        initViews(context);
        iv_icon.setImageResource(icon);
        tv_title.setText(title);
    }

    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.residemenu_item, this);
        iv_icon = findViewById(R.id.iv_icon);
        tv_title = findViewById(R.id.tv_title);
    }

    public ResideMenuItem(Context context, MenuItems menuItems) {
        super(context);
        initViews(context);

        Glide.with(context).load(Constants.IMAGE_PATH + menuItems.getIcon()).into(iv_icon);
        tv_title.setText(menuItems.getName_ar());
    }

    public ResideMenuItem(Context context, int icon, String title) {
        super(context);
        initViews(context);
        iv_icon.setImageResource(icon);
        tv_title.setText(title);
    }

    public void setIcon(int icon) {
        iv_icon.setImageResource(icon);
    }

    public void setTitle(int title) {
        tv_title.setText(title);
    }

    public TextViewCustom getTitle() {
        return tv_title;
    }

    public void setTitle(String title) {
        tv_title.setText(title);
    }
}
