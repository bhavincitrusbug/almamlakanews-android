package ac.news.almamlaka.utilities;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import java.util.List;

/**
 * Created by Mohammad
 * on 1/22/2018 3:18 PM.
 */

public class SocialMediaHelper {

    public static void facebook(Context a, String id) {

        try {
            a.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + id));
            a.startActivity(intent);
        } catch (Exception e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/" + id));
            a.startActivity(intent);

        }
    }

    public static void twitter(Context a, String id) {
        Intent intent = null;
        try {
            a.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=" + id));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + id));
        }
        a.startActivity(intent);
    }

    public static void youtube(Context a, String id) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://user/channel/" + id));
        intent.setComponent(new ComponentName("com.google.android.youtube", "com.google.android.youtube.PlayerActivity"));

        PackageManager manager = a.getPackageManager();
        List<ResolveInfo> info = manager.queryIntentActivities(intent, 0);
        if (info.size() > 0) {
            a.startActivity(intent);
        } else {
            a.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/channel/" + id)));
        }
    }

    public static void instagram(Context a, String id) {
        Uri uri = Uri.parse("http://instagram.com/_u/" + id);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            a.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            a.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + id)));
        }
    }
}
