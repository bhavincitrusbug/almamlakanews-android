package ac.news.almamlaka.utilities.liveStreamCtrlNotification;

/*
 * Created by Mohammad
 * on 10/2/2018 2:55 PM.
 */

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Objects;

import ac.news.almamlaka.R;
import ac.news.almamlaka.ui.activities.MainActivity;
import ac.news.almamlaka.utilities.Constants;

public class NotificationService extends Service {

    Notification status;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String LOG_TAG = "NotificationService";
        switch (Objects.requireNonNull(intent.getAction())) {
            case Constants.ACTION.STARTFOREGROUND_ACTION:
                showNotification();
                break;
            case Constants.ACTION.PREV_ACTION:
                Toast.makeText(this, "Clicked Previous", Toast.LENGTH_SHORT).show();
                Log.i(LOG_TAG, "Clicked Previous");
                break;
            case Constants.ACTION.PLAY_ACTION:
                Toast.makeText(this, "Clicked Play", Toast.LENGTH_SHORT).show();
                Log.i(LOG_TAG, "Clicked Play");
                break;
            case Constants.ACTION.NEXT_ACTION:
                Toast.makeText(this, "Clicked Next", Toast.LENGTH_SHORT).show();
                Log.i(LOG_TAG, "Clicked Next");
                break;
            case Constants.ACTION.STOPFOREGROUND_ACTION:
                Log.i(LOG_TAG, "Received Stop Foreground Intent");
                stopForeground(true);
                stopSelf();
                break;
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void showNotification() {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(MainActivity.class.getCanonicalName());
        intent.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
        //        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 4, intent, PendingIntent.FLAG_CANCEL_CURRENT);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, getString(R.string.live_streaming))//0
                .setSmallIcon(R.mipmap.logo_almamlaka) //0
                .setContentTitle(getString(R.string.app_name)) //0
                .setContentText(getString(R.string.live_streaming_close))//0
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)//0
                .setContentIntent(pendingIntent)//0
                .setOngoing(true).setAutoCancel(false);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(120, mBuilder.build());

    }
}
