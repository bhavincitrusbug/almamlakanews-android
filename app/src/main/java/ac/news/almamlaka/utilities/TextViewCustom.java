package ac.news.almamlaka.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Mohammad
 * on 9/19/2017 11:15 AM.
 */

public class TextViewCustom extends AppCompatTextView {


    Typeface normal_ar = Typeface.createFromAsset(getContext().getAssets(), Constants.FONT);

    public TextViewCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TextViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init() {
        super.setTypeface(normal_ar);

        if (this.getContext() != null) {
            this.setTextSize(getMyTextSize());
        }

    }

    float getMyTextSize() {
        return this.getTextSize() / getResources().getDisplayMetrics().scaledDensity + new PrefManager().getFontSize(this.getContext());
    }

    public TextViewCustom(Context context) {
        super(context);
        init();
    }
}
