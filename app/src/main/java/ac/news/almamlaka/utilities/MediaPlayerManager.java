package ac.news.almamlaka.utilities;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.IOException;

/**
 * Created by Mohammad
 * on 10/7/2018 11:56 AM.
 */
public class MediaPlayerManager implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    public final static boolean IS_PLAYING = true;
    public final static boolean IS_STOPPED = false;
    private static final String MEDIA_PLAYER = "Media Player";
    private static State currentState;
    private final String UrlStreaming;
    private ProgressDialog mProgressDialog;
    private MediaPlayer mMediaPlayer = null;

    public MediaPlayerManager(AppCompatActivity mActivity, final String urlStreaming) {
        if (urlStreaming == null) {
            throw new NullPointerException();
        }
        mProgressDialog = new ProgressDialog(mActivity);
        this.UrlStreaming = urlStreaming;
        currentState = State.NULL;
        createMediaPlayer();
    }


    private void createMediaPlayer() {
        // Creating new media player
        mMediaPlayer = new MediaPlayer();

        // changing media player state
        currentState = State.IDLE;

        // Register a callback to be invoked when a seek operation has been completed.
        mMediaPlayer.setOnPreparedListener(this);
        // Register a callback to be invoked when an error has happened during an asynchronous operation.
        mMediaPlayer.setOnErrorListener(this);

        configureMediaPlayer();
    }

    private void configureMediaPlayer() {
        // initialize mMediaPlayer if it is null
        if (currentState == State.NULL) {
            createMediaPlayer();
        }

        // reset mMediaPlayer if is not in the IDLE state
        if (currentState != State.IDLE) {
            mMediaPlayer.reset();
        }

        // mMediaPlayer in IDLE state

        try {
            // Setting the data source to use.
            mMediaPlayer.setDataSource(UrlStreaming);
        } catch (IOException e) {
            Log.e(MEDIA_PLAYER, "IOException: " + e.toString());

            errorMediaPlayer(mMediaPlayer, MediaPlayer.MEDIA_ERROR_IO, MediaPlayer.MEDIA_ERROR_UNKNOWN);

        } catch (IllegalStateException e) {
            Log.e(MEDIA_PLAYER, "IllegalStateException: " + e.toString());

            errorMediaPlayer(mMediaPlayer, MediaPlayer.MEDIA_ERROR_IO, MediaPlayer.MEDIA_ERROR_UNKNOWN);
        } catch (IllegalArgumentException e) {
            Log.e(MEDIA_PLAYER, "IllegalArgumentException: " + e.toString());

            errorMediaPlayer(mMediaPlayer, MediaPlayer.MEDIA_ERROR_IO, MediaPlayer.MEDIA_ERROR_UNKNOWN);
        }

        //update current state
        currentState = State.INITIALIZED;

    }

    private void prepareAsyncMediaPlayer() {
        // initializing mMediaPlayer if in a state  different from  INITIALIZED
        showDialog();

        if (currentState != State.INITIALIZED) {
            if (currentState != State.NULL && currentState != State.IDLE) {
                mMediaPlayer.reset();
            }
            configureMediaPlayer();
        }

        // Preparing the player for playback, asynchronously.
        mMediaPlayer.prepareAsync();

        //update current state
        currentState = State.PREPARING;
    }

    private void errorMediaPlayer(final MediaPlayer arg0, final int arg1, final int arg2) {
        dismissDialog();

        currentState = State.ERROR;
        onError(arg0, arg1, arg2);
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        currentState = State.PREPARED;

        // The MediaPlayer is ready
        mediaPlayer.start();

        currentState = State.STARTED;

        dismissDialog();
    }

    private void dismissDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    private void showDialog() {
        if (mProgressDialog != null)
            mProgressDialog.show();
    }

    @Override
    public boolean onError(final MediaPlayer arg0, final int arg1, final int arg2) {
        configureMediaPlayer();

        //Error handled
        return true;
    }

    public final void start() {
        prepareAsyncMediaPlayer();
    }

    public final void stop() {
        stopMediaPlayer();
    }

    private void stopMediaPlayer() {
        dismissDialog();

        if (mMediaPlayer != null) {
            // Stopping streaming
            mMediaPlayer.stop();
            currentState = State.STOPPED;
        } else {
            currentState = State.NULL;
        }
    }

    protected final void onDestroy() {
        dismissDialog();

        if (mMediaPlayer != null) {
            mMediaPlayer.release();
        }
    }

    public final boolean getStatus() {
        if (currentState == State.STARTED) {
            return true;
        }

        return false;
    }

    private static enum State {NULL, IDLE, INITIALIZED, PREPARING, PREPARED, STARTED, STOPPED, END, ERROR}
}
