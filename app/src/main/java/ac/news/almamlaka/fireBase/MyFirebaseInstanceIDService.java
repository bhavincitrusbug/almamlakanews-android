package ac.news.almamlaka.fireBase;

/**
 * Created by Mohammad
 * on 1/11/2018 4:30 PM.
 */

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.ResponseClass;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]


    @SuppressLint("HardwareIds")
    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}