package ac.news.almamlaka.fireBase;

/**
 * Created by Mohammad
 * on 1/11/2018 4:30 PM.
 */

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Random;

import ac.news.almamlaka.R;
import ac.news.almamlaka.api.ApiInterface;
import ac.news.almamlaka.api.ServiceGenerator;
import ac.news.almamlaka.models.ResponseClass;
import ac.news.almamlaka.ui.activities.MainActivity;
import ac.news.almamlaka.utilities.Constants;
import ac.news.almamlaka.utilities.PrefManager;
import ac.news.almamlaka.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    // [END receive_message]
    int nID = 101;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.             new PrefManager().isNotificationsEnable(getActivity());

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        //// Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData().get("message"));
            if (new PrefManager().isNotificationsEnable(this))
                sendNotification(remoteMessage.getData().get("message"));
        } else if (remoteMessage.getNotification() != null) {
            // Check if message contains a notification payload.
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if (new PrefManager().isNotificationsEnable(this))
                sendNotification(remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    public void sendNotification(String messageBody) {
        nID = getRandomId();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.NOTIFICATIONS_SCREEN_ID, Util.getScreenID(messageBody));
        intent.putExtra(Constants.NOTIFICATIONS_MESSAGE_BODY, (messageBody));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(String.valueOf(nID));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.app_name);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId).setColor(this.getResources().getColor(R.color.colorPrimary)).setSmallIcon(R.mipmap.logo_almamlaka).setContentText(Util.getMessage(messageBody)).setContentTitle(getString(R.string.app_name)).setAutoCancel(true).setSound(defaultSoundUri).setPriority(getRandomId()).setStyle(new NotificationCompat.BigTextStyle().bigText(Util.getMessage(messageBody))).setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(nID, notificationBuilder.build());
        }
        // you should save message without id cuz its akhbar 3ajel, no need for id here
        // new PrefManager().setBodyMessageForNotification(this, getMessage(messageBody));
        if (isRunning(this)) {
            open(messageBody);
            if (notificationManager != null)
                notificationManager.cancel(nID);
        }
    }


    public int getRandomId() {
        Random random = new Random();
        int Low = 99;
        int High = 1000;
        return random.nextInt(High - Low) + Low;
    }

    public boolean isRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }

    private void open(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.NOTIFICATIONS_SCREEN_ID, Util.getScreenID(messageBody));
        intent.putExtra(Constants.NOTIFICATIONS_MESSAGE_BODY, (messageBody));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }


    private void sendRegistrationToServer(String token) {
        String s = Util.getUniquePhoneIdentity();
        Log.e("UniquePhoneIdentity: ", s);

        ApiInterface apiService = ServiceGenerator.createServiceNotification(ApiInterface.class);
        Call<ResponseClass> callArrayListChatActive = apiService.registerAll(token, s, "1", Constants.AX_API_key, Constants.APP_ID, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID), true);
        callArrayListChatActive.enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                Log.d(TAG, "onResponse: success" + response.code());
            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                t.printStackTrace();
                Log.d(TAG, "onResponse: onFailure");
            }
        });
        new PrefManager().setRegToken(this, token);
    }

}
