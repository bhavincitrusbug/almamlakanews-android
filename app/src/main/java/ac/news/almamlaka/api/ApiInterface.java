package ac.news.almamlaka.api;

import java.util.ArrayList;

import ac.news.almamlaka.models.LiveStream;
import ac.news.almamlaka.models.MenuItems;
import ac.news.almamlaka.models.News;
import ac.news.almamlaka.models.NotificationCategory;
import ac.news.almamlaka.models.ResponseClass;
import ac.news.almamlaka.models.StaticPages;
import ac.news.almamlaka.models.Videos;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by alialquraan
 * on 1/18/18.
 */


public interface ApiInterface {

    @GET("/api/news/list")
    Call<ArrayList<News>> getNews();

    @GET("/api/news/index")
    Call<News> getArticle(@Query("id") String id);

    @GET("/api/category/index")
    Call<ArrayList<News>> getNewsByCategoryID(@Query("cat_id") int cID, @Query("page") int pg);

    @GET("/api/menu/list")
    Call<ArrayList<MenuItems>> getMenuIcons();

    @GET("/api/news/related")
    Call<ArrayList<News>> getArticlesSeeMore(@Query("news_id") String newsID);

    @GET("/api/site/live_stream")
    Call<LiveStream> getLiveStreamLink();

    @GET("/api/page")
    Call<StaticPages> getLiveStreamLink(@Query("id") int pg_id);

    @GET("/api/videos/list")
    Call<ArrayList<Videos>> getVideosList(@Query("id") String id, @Query("page") String pg);


    @GET("/api/category/list")
    Call<ArrayList<NotificationCategory>> getNotificationCategoryList();

    @FormUrlEncoded
    @POST("/api/notifications/register")
    Call<ResponseClass> registerAll(@Field("register_key") String regKey, @Field("identity_key") String idKey, @Field("platform_id") String pID,
                                    @Field("AX-API-KEY") String ax, @Field("application_id") String appID, @Field("device_serial") String ds, @Field("is_first_registration") Boolean isFirstReg);

    @FormUrlEncoded
    @POST("/api/notifications/unregister")
    Call<ResponseClass> unregisterAll(@Field("platform_id") String pID, @Field("AX-API-KEY") String ax,
                                      @Field("application_id") String aID, @Field("device_serial") String ds);

    @FormUrlEncoded
    @POST("/api/notifications/unregisterByCat")
    Call<ResponseClass> unregisterByCat(@Field("application_id") String appID, @Field("cat_id") String cID, @Field("platform_id") String pID, @Field("AX-API-KEY") String ax, @Field("device_serial") String ds);

    @FormUrlEncoded
    @POST("/api/notifications/registerByCat")
    Call<ResponseClass> registerByCat(@Field("register_key") String regKey, @Field("identity_key") String idKey, @Field("application_id") String appID,
                                      @Field("platform_id") String pID, @Field("cat_id") String cID, @Field("AX-API-KEY") String ax, @Field("device_serial") String ds);

    //@Multipart
    //@POST("/API/userPrescription.php")
    //Call<MyPrescriptionRequest> sendPrescriptionCall(@Part("lang") RequestBody lang, @Part("userId") RequestBody userId, @Part("branchId") RequestBody branchId, @Part MultipartBody.Part prescription);

    //@Multipart
    //@POST("/API/userInsurance.php")
    //Call<ModelAdd_ID> postInsurance(@Part MultipartBody.Part image, @Part("type") RequestBody type, @Part("userId") RequestBody userId, @Part("companyId") RequestBody subject, @Part("ratio") RequestBody message, @Part("validUntil") RequestBody submission_result);

    @GET("/api/application")
    Call<ResponseClass> CheckUpdate(@Query("platform_id") String platform_id, @Query("app_version") String app_version);

}