package ac.news.almamlaka.api;

import android.text.TextUtils;

import java.util.concurrent.TimeUnit;

import ac.news.almamlaka.utilities.Constants;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Mohammad
 * on 1/7/2018 10:29 AM.
 */

public class ServiceGenerator {
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(Constants.TIME_OUT, TimeUnit.MINUTES).readTimeout(Constants.TIME_OUT, TimeUnit.MINUTES).writeTimeout(Constants.TIME_OUT, TimeUnit.MINUTES);
    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(Constants.BASE_URL).addConverterFactory(GsonConverterFactory.create());
    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, Constants.ADMIN, Constants.PASSWORD);
    }

    private static <S> S createService(Class<S> serviceClass, String username, String password) {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(username, password);
            return createService(serviceClass, authToken);
        }
        return createService(serviceClass, Constants.USERNAME, Constants.PASSWORD);
    }

    private static <S> S createService(Class<S> serviceClass, final String authToken) {
        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor = new AuthenticationInterceptor(System.currentTimeMillis() / 1000);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        return retrofit.create(serviceClass);
    }

    public static <S> S createServiceNotification(Class<S> serviceClass) {
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor(System.currentTimeMillis() / 1000);
        if (!httpClient.interceptors().contains(interceptor)) {
            httpClient.addInterceptor(interceptor);
            builder.client(httpClient.build());
            retrofit = builder.build();
        }
        return retrofit.create(serviceClass);
    }
}