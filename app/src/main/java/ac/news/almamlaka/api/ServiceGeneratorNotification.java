package ac.news.almamlaka.api;

import java.util.concurrent.TimeUnit;

import ac.news.almamlaka.utilities.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Mohammad
 * on 1/7/2018 10:29 AM.
 */

public class ServiceGeneratorNotification {
    private static OkHttpClient.Builder httpClient4Notification = new OkHttpClient.Builder();
    private static OkHttpClient client4Notification = new OkHttpClient.Builder().connectTimeout(Constants.TIME_OUT, TimeUnit.MINUTES).readTimeout(Constants.TIME_OUT, TimeUnit.MINUTES).build();
    private static Retrofit.Builder builder4Notification = new Retrofit.Builder().baseUrl(Constants.BASE_URL).client(client4Notification).addConverterFactory(GsonConverterFactory.create());
    private static Retrofit retrofit4Notification = builder4Notification.build();

    public static <S> S createService4Notification(Class<S> serviceClass) {
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor(System.currentTimeMillis() / 1000);
        if (!httpClient4Notification.interceptors().contains(interceptor)) {
            httpClient4Notification.addInterceptor(interceptor);
            builder4Notification.client(httpClient4Notification.build());
            retrofit4Notification = builder4Notification.build();
        }
        return retrofit4Notification.create(serviceClass);
    }
}
