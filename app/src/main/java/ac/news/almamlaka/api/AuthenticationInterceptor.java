package ac.news.almamlaka.api;

import android.util.Log;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ac.news.almamlaka.utilities.Constants;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Mohammad
 * on 1/7/2018 9:29 AM.
 */

public class AuthenticationInterceptor implements Interceptor {

    private Long tsLong;

    AuthenticationInterceptor(Long timeStamp) {
        this.tsLong = timeStamp;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();
        String s_responce = tsLong + ":" + Constants.PK + ":" + Constants.USERNAME;
        String sha256 = "";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(s_responce.getBytes("UTF-8"));
            byte[] digest = md.digest();
            sha256 = String.format("%064x", new java.math.BigInteger(1, digest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        Log.d("sim sim", sha256);
        String authorization = "ts=\"" + tsLong + "\" ,response=\"" + sha256 + "\"";
        Request.Builder builder = original.newBuilder().header("Authorization", authorization);
        Log.d("key", authorization);

        Request request = builder.build();
        return chain.proceed(request);
    }
}
