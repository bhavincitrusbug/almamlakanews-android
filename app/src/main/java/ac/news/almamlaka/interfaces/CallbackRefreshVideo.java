package ac.news.almamlaka.interfaces;

/**
 * Created by alialquraan
 * on 2/12/18.
 */

public interface CallbackRefreshVideo {
    void reloadVideoPlayer(String url);
}
